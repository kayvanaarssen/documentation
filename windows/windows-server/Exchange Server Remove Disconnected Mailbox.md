Check the mailbox database for disconnected mailboxes:

Open the Exchange Management Shell

Execute the following command and change the `DB04` to the name of your Database

`Get-MailboxStatistics -Database “DB04” | where {$_.disconnectdate -ne $null} | select displayname,MailboxGUID`

Next copy the ID of the mailbox and paste it at the end of this command:

Please note: change the name `DB04` the the name of your Database

`Remove-Mailbox -Database “DB04” -StoreMailboxIdentity 49703fc1-e028-471b-b004-7af29f9dc571`
	