Voer eerst het:

`Get-ReceiveConnector | fl Name,messageratelimit`

Commando uit.

Bekijk daarin welke naam de exchange server heeft en hier zie je ook het Message Limit van 5 staan bij de Client Proxy en de Client Frontend

Voer daarna de onderstaande commando’s uit met de gewenste waardes. [Exchange Server Name] = de naam van je exchange server zonder [ —- ]

`Get-ReceiveConnector -Identity "Client Proxy [Exchange Server Name]" | Set-ReceiveConnector -MessageRateLimit 500`

`Get-ReceiveConnector -Identity "Client Frontend [Exchange Server Name]" | Set-ReceiveConnector -MessageRateLimit 500`