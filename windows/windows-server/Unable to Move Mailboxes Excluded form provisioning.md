# Unable to Move Mailboxes to an Exchange Server 2013 Database that is Excluded from Provisioning

>Exchange Server 2013 mailbox databases can be excluded from the automatic mailbox provisioning load balancer, which distributes new mailboxes evenly across the available databases in an Exchange environment.

This can be performed using `Set-MailboxDatabase`, for example:

`Set-MailboxDatabase DB04 -IsExcludedFromProvisioning:$true`

>A bug currently exists in Exchange Server 2013 that prevents mailboxes from being manually moved to a database that is excluded from provisioning.

`New-MoveRequest "mary.friel" -TargetDatabase DB04 -Verbose`

>The database DB04 is excluded from provisioning. Please select a different target database for the move.
>    + CategoryInfo          : InvalidArgument: (mary.friel:MailboxOrMailUserIdParameter) [New-MoveRequest], DatabaseEx
>   clude...ioningException
>    + FullyQualifiedErrorId : [Server=EX2013SRV2,RequestId=a4e02908-2888-488c-a295-ff95fe3b23cf,TimeStamp=18/09/2014 3
>   :23:27 PM] [FailureCategory=Cmdlet-DatabaseExcludedFromProvisioningException] C18F48C0,Microsoft.Exchange.Manageme
>  nt.RecipientTasks.NewMoveRequest
>    + PSComputerName        : ex2013srv2.exchangeserverpro.net

>Normally manual mailbox moves to a database excluded from provisioning would be permitted. Microsoft has confirmed that this is currently a bug and are expecting to include a fix in an upcoming update to Exchange Server 2013.

>In the meantime if you need to move mailboxes to the database you can use the “suspend” instead of “exclude” setting on the database. “Suspend” is intended for situations where an admin only temporarily wants to prevent new mailboxes from being provisioned on the database, but otherwise achieves the same thing.

`Set-MailboxDatabase DB04 -IsExcludedFromProvisioning:$false`

`Set-MailboxDatabase DB04 -IsSuspendedFromProvisioning:$true`

`New-MoveRequest "mary.friel" -TargetDatabase DB04 -Verbose`

```
DisplayName               StatusDetail              TotalMailboxSize          TotalArchiveSize         PercentComplete
-----------               ------------              ----------------          ----------------         ---------------
Mary Friel                Queued                    128.9 MB (135,205,784 ...                          0
```