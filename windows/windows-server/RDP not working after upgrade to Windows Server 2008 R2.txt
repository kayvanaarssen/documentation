After upgrading from Windows Server 2003R2 to Windows Server 2008 R2, I had an issue with one of my network cards.

This network card prevented RDP to the server from working correctly.

When reinstalling the network card drivers after the upgrade I received an error on one of the network cards.
The card couldn’t be installed because the name was already in use. But there was no network card with the same name.

As described in http://support.microsoft.com/kb/823771 I uninstalled all network cards and deleted the corresponding registry entries.
An easier way to find these registry keys is looking at the driver name before uninstalling the card ! This name will in most cases be the name in the service tree.

After a reboot my network cards were all installed correctly so I configured network teaming.

This is when I noticed that the RDP connection was not working!

Netstat -a showed me that there was no service listening on rdp port 3389.

When opening the rdp-tcp properties in the Remote Desktop Session Host Configuration, and going to the Network Adapter tab I got following error:

“Remote Desktop Session Host Configuration tool is not able to obtain the properties for this connection. The connection has either been deleted or the internal state of this connection has been corrupted. Please close all property pages, and select refresh from the menu.”

I was not able to change the network card.
But I did find an entry in the registry to change the network card to all configured network cards:

Navigate to the following value:
HKEY_LOCAL_MACHINE\SYSTEM\CurrentControlSet\Control\Terminal Server\WinStations\RDP-TcpLanAdapter

If you want RDP to listen on all LAN adapters enter value of 0.

I restarted the Remote Desktop service, just to be sure. And RDP connections to my server were successfully again!

The value was probably set to the network card that didn’t install properly. RDP was trying to connect via a network card which didn’t exist anymore.

Source: http://scug.be/valerie/2010/10/14/rdp-not-working-after-upgrade-to-windows-server-2008-r2/