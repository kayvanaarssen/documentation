I see monitoring mailboxes and there must be an AuditLog mailbox too...

`Get-Mailbox -Database EX2016-01-DB01 -Monitoring`

`Get-Mailbox -Database EX2016-01-DB01 -AuditLog`

Move it to another database before removing the DB...

`Get-Mailbox -Database EX2016-01-DB01 -AuditLog | New-MoveRequest –TargetDatabase New-E2016-DBName`