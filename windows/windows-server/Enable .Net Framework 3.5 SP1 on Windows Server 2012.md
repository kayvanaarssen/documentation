While playing with the new Windows Server 2012 and Windows 8 Client I found out that the .Net Framework 3.5 SP1 option was grey out and not available. After doing some digging and reading several forum I found the answer.

Steps to enable .Net Framework 3.5 SP1 in Windows Server 2012 1. Open a Command Prompt with Administrator Priveledges
2. type: 

`dism /online /enable-feature /all /featurename:netfx3 /source:d:\sources\sxs /limitaccess`