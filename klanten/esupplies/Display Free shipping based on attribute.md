Edit the view.phtml file in /domains/sextoyland.nl/public_html/app/design/frontend/smartwave/porto/template/catalog/product

Add the following lines where you want the Free Shipping block to be displayed.

```
<!-- Add Free shipping Attribuut ICTWebSolution -->
<div class="free-shipping-product-page">
<?php $attributeValue = $_product->getAttributeText('free_shipping'); ?>
<?php $staticBlockIdentifier = $attributeValue; ?>
<span><?php echo $this->getLayout()->createBlock('cms/block')->setBlockId($staticBlockIdentifier)->toHtml();?></span>
</div>
<!-- Add Free shipping Attribuut ICTWebSolution -->
```

CSS Code: 

```
.free-shipping-product-page {
  h3{
    color: #3d3d3d;
    margin-top: 15px; 
    margin-bottom:-10px;
  }
  strong{
    color: #00b900;
  }
}
```
