```
<!-- Display Static Block based on Attribute (USP Keuze) ICTWebsolution --> 
			<div class="usp-list">
				<?php $attributeValue = $_product->getAttributeText('usp_block_product'); ?>
				<?php $staticBlockIdentifier = $attributeValue; ?>

				<?php echo $this->getLayout()->createBlock('cms/block')->setBlockId($staticBlockIdentifier)->toHtml();?>
			</div>
			<!-- Display Static Block based on Attribute (USP Keuze) ICTWebsolution --> 
````