Add the following to this file (list.phtml): 

`domains/wheelstandcentre.nl/public_html/app/design/frontend/ultimo/wheelstandcentre/template/catalog/product/list.phtml`

and

`domains/wheelstandcentre.nl/public_html/app/design/frontend/ultimo/mardomedia/template/catalog/product/list.phtml`

```
<!-- Display Delivery Time on Category Page ICTWebSolution -->
<div class="category-below-addtocart-deliverytime">
<?php $attributeValue = $_product->getAttributeText('usp_block_product'); ?>
<?php $voorraadStatus = $attributeValue;?>
<?php if ($voorraadStatus == product_page_usp_list):?>
	<span><?php echo $this->getLayout()->createBlock('cms/block')->setBlockId('category_delevery_time_monta')->toHtml();?></span>
<?php endif; ?>
<?php if ($voorraadStatus == product_page_usp_list_almere):?>
	<span><?php echo $this->getLayout()->createBlock('cms/block')->setBlockId('category_delevery_time_almere')->toHtml();?></span>
<?php endif; ?>
</div>
<!-- END Display Delivery Time on Category Page ICTWebSolution -->
```

CSS Code:

```
.category-below-addtocart-deliverytime{
	span{
		color: #009900;
	}
}
```