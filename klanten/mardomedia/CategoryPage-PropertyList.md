Add the following to this file (list.phtml): 

`domains/wheelstandcentre.nl/public_html/app/design/frontend/ultimo/wheelstandcentre/template/catalog/product/list.phtml`

and

`domains/wheelstandcentre.nl/public_html/app/design/frontend/ultimo/mardomedia/template/catalog/product/list.phtml`

```
<!-- Display Product Options on Category Page ICTWebsolution --> 
<div class="category-below-addtocart">
	<?php echo $_helper->productAttribute($_product, $_product->getProductGridProperty(), 'product_grid_property') ?>
</div>
<!-- Display Product Options on Category Page ICTWebsolution --> 
```

CSS Code:

```
.category-below-addtocart {
	li:before {
	  font-size: 0.8rem;
	  content: "\25CF";
	  left: 0px;
	  position: inherit;
	  margin-right: 5px;
	}
	li {
		font-weight: bold;
	}
}
```