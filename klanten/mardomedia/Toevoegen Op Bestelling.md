Toevoegen Op Bestelling.md

/Volumes/Wheelstandcentre.nl/domains/wheelstandcentre.nl/public_html/app/design/frontend/ultimo/mardomedia/template/catalog/product/view/type/default.phtml

```
<?php
/**
 * Magento
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Academic Free License (AFL 3.0)
 * that is bundled with this package in the file LICENSE_AFL.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/afl-3.0.php
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to license@magentocommerce.com so we can send you a copy immediately.
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade Magento to newer
 * versions in the future. If you wish to customize Magento for your
 * needs please refer to http://www.magentocommerce.com for more information.
 *
 * @category    design
 * @package     base_default
 * @copyright   Copyright (c) 2012 Magento Inc. (http://www.magentocommerce.com)
 * @license     http://opensource.org/licenses/afl-3.0.php  Academic Free License (AFL 3.0)
 */
?>
<?php /* @var $this Mage_Catalog_Block_Product_View_Abstract */?>
<?php $_product = $this->getProduct() ?>

<?php echo $this->getChildHtml('product_type_data_extra') ?>
<?php echo $this->getPriceHtml($_product) ?>
<?php if ($this->displayProductStockStatus()): ?>
	<?php if ($_product->isAvailable()): ?>
	    <p class="availability in-stock"><i class="fa fa-check"></i><span><?php echo $this->__('On stock in central warehouse') // In stock aangepast naar In Stock - Central Warehouse ?></span></p>
	    <meta itemprop="availability" content="http://schema.org/InStock">
	<?php else: ?> 
	    <p class="availability out-of-stock"><span><?php echo $this->__('Out of stock in central warehouse') // Out of stock aangepast naar Out of Stock - Central Warehouse ?></span></p>
	    <meta itemprop="availability" content="http://schema.org/OutOfStock">
	<?php endif; ?>
<?php endif; ?>
<!-- Display Static Block based on Stock Alemere Status ICTWebsolution --> 
<div class="almere-stock-status">
	<?php $attributeValue = $_product->getAttributeText('stock_almere'); ?>
	<?php $staticBlockIdentifier = $attributeValue; ?>
	<?php echo $this->getLayout()->createBlock('cms/block')->setBlockId($staticBlockIdentifier)->toHtml();?>
</div>
<!-- Display Static Block based on Stock Alemere Status ICTWebsolution --> 

<?php // Check for store stock
$stores = Mage::app()->getStore()->getStoreId();
$nlStore = array(1, 6, 10, 12);
if (in_array($stores, $nlStore)):
	$storestock = $_product->getResource()->getAttribute('store_stock');
		if ($storestock->getFrontend()->getValue($_product) >= '1'):?>
			<span><a href="/contact">Ook beschikbaar in onze winkel</a></span>
<?php endif;
endif;
?>

```

/Volumes/Wheelstandcentre.nl/domains/wheelstandcentre.nl/public_html/app/design/frontend/ultimo/mardomedia/template/catalog/product/view/addtocart.phtml

```
<?php
/**
 * Magento
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Academic Free License (AFL 3.0)
 * that is bundled with this package in the file LICENSE_AFL.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/afl-3.0.php
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to license@magentocommerce.com so we can send you a copy immediately.
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade Magento to newer
 * versions in the future. If you wish to customize Magento for your
 * needs please refer to http://www.magentocommerce.com for more information.
 *
 * @category    design
 * @package     base_default
 * @copyright   Copyright (c) 2012 Magento Inc. (http://www.magentocommerce.com)
 * @license     http://opensource.org/licenses/afl-3.0.php  Academic Free License (AFL 3.0)
 */
?>
<?php $_product = $this->getProduct(); ?>
<?php $buttonTitle = $this->__('Add to Cart'); ?>
<?php $inventory =  Mage::getModel('cataloginventory/stock_item')->loadByProduct($_product); ?>
<!-- Display Op Bestelling and Change Cart button ICTWebSolution -->
<?php $attributeValue = $_product->getAttributeText('op_bestelling'); ?>
<?php $staticBlockIdentifier = $attributeValue; ?>
<?php
/** also add: 
	<?php if ($attributeValue == op_bestelling_yes){echo 'op-bestelling';}?> to the class
<!-- END Display Op Bestelling and Change Cart button ICTWebSolution -->
*/
?>
<?php if ($_product->isSaleable()): ?>
    <div class="add-to-cart left-side">
      <button type="button" title="<?php echo $buttonTitle ?>" id="product-addtocart-button" class="button btn-cart <?php if ($attributeValue == op_bestelling_yes){echo 'op-bestelling';}?> <?php if ($inventory->getBackorders() == '101'){ echo 'pre-order';}?>" onclick="productAddToCartForm.submit(this)">
        <span class="buy-product-view">
          <!--<span><?php echo $buttonTitle ?></span>-->
        <!-- Display Op Bestelling and Change Cart button ICTWebSolution -->
        <span>
          	<?php if ($attributeValue == op_bestelling_yes):?><?php echo $this->__('Op Bestelling') ?>
          	<?php elseif ($attributeValue = op_bestelling_no):{ echo $buttonTitle;} ?>
  			<?php endif;?>
  		</span>
  		<!-- END Display Op Bestelling and Change Cart button ICTWebSolution -->
          <i class="fa fa-size fa-shopping-cart"></i>
        </span>
      </button>
    </div>
    <div class="paypal-wrapper"><?php echo $this->getChildHtml('', true, true) ?></div>
<?php endif; ?>
<!-- Display Op Bestelling and Change Cart button ICTWebSolution -->
<?php if ($attributeValue == op_bestelling_yes):?>
	<?php echo $this->getLayout()->createBlock('cms/block')->setBlockId('op_bestelling_yes')->toHtml();?>
<?php endif; ?>
<!-- END Display Op Bestelling and Change Cart button ICTWebSolution -->

```

# CSS

```

.op-bestelling {
    background: #4b4b4b!important;
    border: 1px solid #7f7251!important;
    box-shadow: inset 0 -2px 0 0 #7f7251!important;
    font-size: 18px;
    line-height: 45px;
    border-radius: 3px;
    color: #ffffff!important;
}
.info-box-add-to-cart {
    display: block;
    width: 300px;
    padding: 10px;
    background: #efe;
    border-radius: 3px;
    margin-top: 10px;
}
.info-box-add-to-cart-page {
    display: block;
    width: 85%;
    padding: 10px;
    background: rgba(255, 88, 90, 0.570);
    border-radius: 3px;
    margin-top: 10px;
}
```

## CONCEPT 

```
<?php
/**
 * Magento
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Academic Free License (AFL 3.0)
 * that is bundled with this package in the file LICENSE_AFL.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/afl-3.0.php
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to license@magentocommerce.com so we can send you a copy immediately.
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade Magento to newer
 * versions in the future. If you wish to customize Magento for your
 * needs please refer to http://www.magentocommerce.com for more information.
 *
 * @category    design
 * @package     base_default
 * @copyright   Copyright (c) 2012 Magento Inc. (http://www.magentocommerce.com)
 * @license     http://opensource.org/licenses/afl-3.0.php  Academic Free License (AFL 3.0)
 */
?>
<?php /* @var $this Mage_Catalog_Block_Product_View_Abstract */?>
<?php $_product = $this->getProduct() ?>
<!-- Display Op Bestelling and Change Cart button ICTWebSolution -->
<?php $opbestelling = 'op_bestelling_yes' ?>
<?php $opbestelling_no = 'op_bestelling_no' ?>
<?php $attributeValue = $_product->getAttributeText('op_bestelling'); ?>
<!-- END Display Op Bestelling and Change Cart button ICTWebSolution -->

<?php echo $this->getChildHtml('product_type_data_extra') ?>
<?php echo $this->getPriceHtml($_product) ?>
<!-- Display Op Bestelling and Change Cart button ICTWebSolution -->
<?php if ($attributeValue == $opbestelling):?>
	<p class="availability in-stock"><i class="fa fa-check"></i><span><?php echo $this->__('In Stock - At Supplier') ?></span></p>
	    <meta itemprop="availability" content="http://schema.org/InStock">
<?php endif; ?>
<!-- END Display Op Bestelling and Change Cart button ICTWebSolution -->
<!-- Display Op Bestelling and Change Cart button ICTWebSolution -->

<?php if (!$opbestelling):?>
	<?php if ($this->displayProductStockStatus()): ?>
		<?php if ($_product->isAvailable() && ($opbestelling_no)):?>
		    <p class="availability in-stock"><i class="fa fa-check"></i><span><?php echo $this->__('In stock - Central Warehouse') // In stock aangepast naar In Stock - Central Warehouse ?></span></p>
		    <meta itemprop="availability" content="http://schema.org/InStock">
		<?php else: ?> 
		    <p class="availability out-of-stock"><span><?php echo $this->__('Out of stock - Central Warehouse') // Out of stock aangepast naar Out of Stock - Central Warehouse ?></span></p>
		    <meta itemprop="availability" content="http://schema.org/OutOfStock">
		<?php endif; ?>
	<?php endif; ?>
<?php endif; ?>
<?php if ($attributeValue == op_bestelling_no):?>
	<?php if ($this->displayProductStockStatus()): ?>
		<?php if ($_product->isAvailable() && ($opbestelling_no)):?>
		    <p class="availability in-stock"><i class="fa fa-check"></i><span><?php echo $this->__('In stock - Central Warehouse') // In stock aangepast naar In Stock - Central Warehouse ?></span></p>
		    <meta itemprop="availability" content="http://schema.org/InStock">
		<?php else: ?> 
		    <p class="availability out-of-stock"><span><?php echo $this->__('Out of stock - Central Warehouse') // Out of stock aangepast naar Out of Stock - Central Warehouse ?></span></p>
		    <meta itemprop="availability" content="http://schema.org/OutOfStock">
		<?php endif; ?>
	<?php endif; ?>
<?php endif; ?>
<?php if ($attributeValue == 0):?>
	<?php if ($this->displayProductStockStatus()): ?>
		<?php if ($_product->isAvailable() && ($opbestelling_no)):?>
		    <p class="availability in-stock"><i class="fa fa-check"></i><span><?php echo $this->__('In stock - Central Warehouse') // In stock aangepast naar In Stock - Central Warehouse ?></span></p>
		    <meta itemprop="availability" content="http://schema.org/InStock">
		<?php else: ?> 
		    <p class="availability out-of-stock"><span><?php echo $this->__('Out of stock - Central Warehouse') // Out of stock aangepast naar Out of Stock - Central Warehouse ?></span></p>
		    <meta itemprop="availability" content="http://schema.org/OutOfStock">
		<?php endif; ?>
	<?php endif; ?>
<?php endif; ?>
<!-- END Display Op Bestelling and Change Cart button ICTWebSolution -->

<!-- Display Static Block based on Stock Alemere Status ICTWebsolution --> 
<div class="almere-stock-status">
	<?php $attributeValue = $_product->getAttributeText('stock_almere'); ?>
	<?php $staticBlockIdentifier = $attributeValue; ?>
	<?php echo $this->getLayout()->createBlock('cms/block')->setBlockId($staticBlockIdentifier)->toHtml();?>
</div>
<!-- Display Static Block based on Stock Alemere Status ICTWebsolution --> 

<?php // Check for store stock
$stores = Mage::app()->getStore()->getStoreId();
$nlStore = array(1, 6, 10, 12);
if (in_array($stores, $nlStore)):
	$storestock = $_product->getResource()->getAttribute('store_stock');
		if ($storestock->getFrontend()->getValue($_product) >= '1'):?>
			<span><a href="/contact">Ook beschikbaar in onze winkel</a></span>
<?php endif;
endif;
?>

```

/Volumes/dev.wheelstandcentre.nl/domains/dev.wheelstandcentre.nl/public_html/app/design/frontend/ultimo/default/template/checkout/cart/item/default.phtml

# CART WARNING

```
<?php
/**
 * Magento
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Academic Free License (AFL 3.0)
 * that is bundled with this package in the file LICENSE_AFL.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/afl-3.0.php
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to license@magentocommerce.com so we can send you a copy immediately.
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade Magento to newer
 * versions in the future. If you wish to customize Magento for your
 * needs please refer to http://www.magentocommerce.com for more information.
 *
 * @category    design
 * @package     base_default
 * @copyright   Copyright (c) 2012 Magento Inc. (http://www.magentocommerce.com)
 * @license     http://opensource.org/licenses/afl-3.0.php  Academic Free License (AFL 3.0)
 */
?>
<?php
/**
 * Added classes (names) for columns and cell labels
 */
?>
<?php
$_item = $this->getItem();
$isVisibleProduct = $_item->getProduct()->isVisibleInSiteVisibility();
$canApplyMsrp = Mage::helper('catalog')->canApplyMsrp($_item->getProduct(), Mage_Catalog_Model_Product_Attribute_Source_Msrp_Type::TYPE_BEFORE_ORDER_CONFIRM);

//Default image size
$imgWidth = 168;
$imgHeight = 168;

    //Image aspect ratio
    if ($this->helper('ultimo')->getCfg('category/aspect_ratio'))
    {
        $imgHeight = 0; //Height will be calculated automatically (based on width) to keep the aspect ratio
    }
?>
<tr>
    <td><?php if ($this->hasProductUrl()):?><a href="<?php echo $this->getProductUrl() ?>" title="<?php echo $this->htmlEscape($this->getProductName()) ?>" class="product-image"><?php endif;?><img src="<?php echo $this->getProductThumbnail()->resize(168); ?>" alt="<?php echo $this->htmlEscape($this->getProductName()) ?>" /><?php if ($this->hasProductUrl()):?></a><?php endif;?></td>
    <td>
    <?php /* TODO
    <td class="col-img"><?php if ($this->hasProductUrl()):?><a href="<?php echo $this->getProductUrl() ?>" title="<?php echo $this->escapeHtml($this->getProductName()) ?>" class="product-image"><?php endif;?><img src="<?php echo $this->helper('infortis/image')->getImg($_item->getProduct(), $imgWidth, $imgHeight, 'thumbnail'); ?>" alt="<?php echo $this->escapeHtml($this->getProductName()) ?>" /><?php if ($this->hasProductUrl()):?></a><?php endif;?></td>
    <td>
    */ ?>
        <h2 class="product-name">
        <?php if ($this->hasProductUrl()):?>
            <a href="<?php echo $this->getProductUrl() ?>"><?php echo $this->escapeHtml($this->getProductName()) ?></a>
        <?php else: ?>
            <?php echo $this->escapeHtml($this->getProductName()) ?>
        <?php endif; ?>
        </h2>

        <!--- Display Op Bestelling Warning - ICTWebSolution -->
        <?php $_product = Mage::getModel('catalog/product')->load($_item->getProduct()->getId());?>
        <?php $attributeValue = $_product->getAttributeText('op_bestelling'); ?>
        <?php if ($attributeValue == op_bestelling_yes):?>
            <?php echo $this->getLayout()->createBlock('cms/block')->setBlockId('op_bestelling_cart_page')->toHtml();?>
        <?php endif; ?>
        <!-- END Display Op Bestelling Warning - ICTWebSolution -->
        
        <?php if ($_options = $this->getOptionList()):?>
        <dl class="item-options">
            <?php foreach ($_options as $_option) : ?>
            <?php $_formatedOptionValue = $this->getFormatedOptionValue($_option) ?>
            <dt><?php echo $this->escapeHtml($_option['label']) ?></dt>
            <dd<?php if (isset($_formatedOptionValue['full_view'])): ?> class="truncated"<?php endif; ?>><?php echo $_formatedOptionValue['value'] ?>
                <?php if (isset($_formatedOptionValue['full_view'])): ?>
                <div class="truncated_full_value">
                    <dl class="item-options">
                        <dt><?php echo $this->escapeHtml($_option['label']) ?></dt>
                        <dd><?php echo $_formatedOptionValue['full_view'] ?></dd>
                    </dl>
                </div>
                <?php endif; ?>
            </dd>
            <?php endforeach; ?>
        </dl>
        <?php endif;?>
        <?php if ($messages = $this->getMessages()): ?>
        <?php foreach ($messages as $message): ?>
            <p class="item-msg <?php echo $message['type'] ?>">* <?php echo $this->escapeHtml($message['text']) ?></p>
        <?php endforeach; ?>
        <?php endif; ?>
        <?php $addInfoBlock = $this->getProductAdditionalInformationBlock(); ?>
        <?php if ($addInfoBlock): ?>
            <?php echo $addInfoBlock->setItem($_item)->toHtml() ?>
        <?php endif;?>
    </td>
    <td class="col-edit a-center">
        <?php if ($isVisibleProduct): ?>
        <a href="<?php echo $this->getConfigureUrl() ?>" title="<?php echo Mage::helper('core')->quoteEscape($this->__('Edit item parameters')) ?>"><?php echo $this->__('Edit') ?></a>
        <?php endif ?>
    </td>
    <?php if ($this->helper('wishlist')->isAllowInCart()) : ?>
    <td class="col-wish a-center">
    
        <span class="cell-label"><?php echo $this->__('Move to Wishlist') ?></span>
    
        <?php if ($isVisibleProduct): ?>
            <input type="checkbox" value="1" name="cart[<?php echo $_item->getId() ?>][wishlist]" title="<?php echo $this->__('Move to Wishlist') ?>" class="checkbox" />
            <?php /*?><a href="<?php echo $this->helper('wishlist')->getMoveFromCartUrl($_item->getId()); ?>" class="link-wishlist use-ajax"><?php echo $this->__('Move'); ?></a><?php */?>
        <?php endif ?>
    </td>
    <?php endif ?>

    <?php if ($canApplyMsrp): ?>
        <td class="col-msrp a-right"<?php if ($this->helper('tax')->displayCartBothPrices()): ?> colspan="2"<?php endif; ?>>
            <span class="cart-price">
                <span class="cart-msrp-unit"><?php echo $this->__('See price before order confirmation.'); ?></span>
                <?php $helpLinkId = 'cart-msrp-help-' . $_item->getId(); ?>
                <a id="<?php echo $helpLinkId ?>" href="#" class="map-help-link"><?php echo $this->__("What's this?"); ?></a>
                <script type="text/javascript">
                    Catalog.Map.addHelpLink($('<?php echo $helpLinkId ?>'), "<?php echo $this->__("What's this?") ?>");
                </script>
            </span>
        </td>
    <?php else: ?>

        <?php if ($this->helper('tax')->displayCartPriceExclTax() || $this->helper('tax')->displayCartBothPrices()): ?>
        <td class="col-unit-price a-right">
        
            <span class="cell-label">
                <?php echo $this->__('Unit Price') ?>
                <?php if ($this->helper('tax')->displayCartBothPrices()): ?>
                    <?php echo $this->helper('tax')->getIncExcTaxLabel(false) ?>
                <?php endif; ?>
            </span>
        
            <?php if (Mage::helper('weee')->typeOfDisplay($_item, array(1, 4), 'sales') && $_item->getWeeeTaxAppliedAmount()): ?>
                <span class="cart-tax-total" onclick="taxToggle('eunit-item-tax-details<?php echo $_item->getId(); ?>', this, 'cart-tax-total-expanded');">
            <?php else: ?>
                <span class="cart-price">
            <?php endif; ?>
                <?php if (Mage::helper('weee')->typeOfDisplay($_item, array(0, 1, 4), 'sales') && $_item->getWeeeTaxAppliedAmount()): ?>
                    <?php echo $this->helper('checkout')->formatPrice($_item->getCalculationPrice()+$_item->getWeeeTaxAppliedAmount()+$_item->getWeeeTaxDisposition()); ?>
                <?php else: ?>
                    <?php echo $this->helper('checkout')->formatPrice($_item->getCalculationPrice()) ?>
                <?php endif; ?>

            </span>


            <?php if (Mage::helper('weee')->getApplied($_item)): ?>

                <div class="cart-tax-info" id="eunit-item-tax-details<?php echo $_item->getId(); ?>" style="display:none;">
                    <?php if (Mage::helper('weee')->typeOfDisplay($_item, 1, 'sales') && $_item->getWeeeTaxAppliedAmount()): ?>
                        <?php foreach (Mage::helper('weee')->getApplied($_item) as $tax): ?>
                            <span class="weee"><?php echo $tax['title']; ?>: <?php echo Mage::helper('checkout')->formatPrice($tax['amount'],true,true); ?></span>
                        <?php endforeach; ?>
                    <?php elseif (Mage::helper('weee')->typeOfDisplay($_item, 2, 'sales') && $_item->getWeeeTaxAppliedAmount()): ?>
                        <?php foreach (Mage::helper('weee')->getApplied($_item) as $tax): ?>
                            <span class="weee"><?php echo $tax['title']; ?>: <?php echo Mage::helper('checkout')->formatPrice($tax['amount'],true,true); ?></span>
                        <?php endforeach; ?>
                    <?php elseif (Mage::helper('weee')->typeOfDisplay($_item, 4, 'sales') && $_item->getWeeeTaxAppliedAmount()): ?>
                        <?php foreach (Mage::helper('weee')->getApplied($_item) as $tax): ?>
                            <span class="weee"><?php echo $tax['title']; ?>: <?php echo Mage::helper('checkout')->formatPrice($tax['amount'],true,true); ?></span>
                        <?php endforeach; ?>
                    <?php endif; ?>
                </div>

                <?php if (Mage::helper('weee')->typeOfDisplay($_item, 2, 'sales') && $_item->getWeeeTaxAppliedAmount()): ?>
                    <div class="cart-tax-total" onclick="taxToggle('eunit-item-tax-details<?php echo $_item->getId(); ?>', this, 'cart-tax-total-expanded');">
                        <span class="weee"><?php echo Mage::helper('weee')->__('Total'); ?>: <?php echo $this->helper('checkout')->formatPrice($_item->getCalculationPrice()+$_item->getWeeeTaxAppliedAmount()+$_item->getWeeeTaxDisposition()); ?></span>
                    </div>
                <?php endif; ?>
            <?php endif; ?>
        </td>
        <?php endif; ?><!-- inclusive price starts here -->
        <?php if ($this->helper('tax')->displayCartPriceInclTax() || $this->helper('tax')->displayCartBothPrices()): ?>
        <td class="col-unit-price">
        
            <span class="cell-label">
                <?php echo $this->__('Unit Price') ?>
                <?php if ($this->helper('tax')->displayCartBothPrices()): ?>
                    <?php echo $this->helper('tax')->getIncExcTaxLabel(true) ?>
                <?php endif; ?>
            </span>
        
            <?php $_incl = $this->helper('checkout')->getPriceInclTax($_item); ?>
            <?php if (Mage::helper('weee')->typeOfDisplay($_item, array(1, 4), 'sales') && $_item->getWeeeTaxAppliedAmount()): ?>
                <span class="cart-tax-total" onclick="taxToggle('unit-item-tax-details<?php echo $_item->getId(); ?>', this, 'cart-tax-total-expanded');">
            <?php else: ?>
                <span class="cart-price">
            <?php endif; ?>

                <?php if (Mage::helper('weee')->typeOfDisplay($_item, array(0, 1, 4), 'sales') && $_item->getWeeeTaxAppliedAmount()): ?>
                    <?php echo $this->helper('checkout')->formatPrice($_incl + Mage::helper('weee')->getWeeeTaxInclTax($_item)); ?>
                <?php else: ?>
                    <?php echo $this->helper('checkout')->formatPrice($_incl-$_item->getWeeeTaxDisposition()) ?>
                <?php endif; ?>

            </span>
            <?php if (Mage::helper('weee')->getApplied($_item)): ?>

                <div class="cart-tax-info" id="unit-item-tax-details<?php echo $_item->getId(); ?>" style="display:none;">
                    <?php if (Mage::helper('weee')->typeOfDisplay($_item, 1, 'sales') && $_item->getWeeeTaxAppliedAmount()): ?>
                        <?php foreach (Mage::helper('weee')->getApplied($_item) as $tax): ?>
                            <span class="weee"><?php echo $tax['title']; ?>: <?php echo Mage::helper('checkout')->formatPrice($tax['amount_incl_tax'],true,true); ?></span>
                        <?php endforeach; ?>
                    <?php elseif (Mage::helper('weee')->typeOfDisplay($_item, 2, 'sales') && $_item->getWeeeTaxAppliedAmount()): ?>
                        <?php foreach (Mage::helper('weee')->getApplied($_item) as $tax): ?>
                            <span class="weee"><?php echo $tax['title']; ?>: <?php echo Mage::helper('checkout')->formatPrice($tax['amount_incl_tax'],true,true); ?></span>
                        <?php endforeach; ?>
                    <?php elseif (Mage::helper('weee')->typeOfDisplay($_item, 4, 'sales') && $_item->getWeeeTaxAppliedAmount()): ?>
                        <?php foreach (Mage::helper('weee')->getApplied($_item) as $tax): ?>
                            <span class="weee"><?php echo $tax['title']; ?>: <?php echo Mage::helper('checkout')->formatPrice($tax['amount_incl_tax'],true,true); ?></span>
                        <?php endforeach; ?>
                    <?php endif; ?>
                </div>

                <?php if (Mage::helper('weee')->typeOfDisplay($_item, 2, 'sales') && $_item->getWeeeTaxAppliedAmount()): ?>
                    <div class="cart-tax-total" onclick="taxToggle('unit-item-tax-details<?php echo $_item->getId(); ?>', this, 'cart-tax-total-expanded');">
                        <span class="weee"><?php echo Mage::helper('weee')->__('Total incl. tax'); ?>: <?php echo $this->helper('checkout')->formatPrice($_incl + Mage::helper('weee')->getWeeeTaxInclTax($_item)); ?></span>
                    </div>
                <?php endif; ?>
            <?php endif; ?>
        </td>
        <?php endif; ?>
    <?php endif; ?>
    <td class="a-center">
        
        <span class="cell-label"><?php echo $this->__('Qty') ?></span>
    
        <input name="cart[<?php echo $_item->getId() ?>][qty]" 
               data-cart-item-id="<?php echo $this->jsQuoteEscape($_item->getSku()) ?>"
               value="<?php echo $this->getQty() ?>" size="4" title="<?php echo Mage::helper('core')->quoteEscape($this->__('Qty')) ?>" class="input-text qty" maxlength="12" />
    </td>
    <!--Sub total starts here -->
    <?php if (($this->helper('tax')->displayCartPriceExclTax() || $this->helper('tax')->displayCartBothPrices()) && !$_item->getNoSubtotal()): ?>
    <td class="<?php if($this->helper('tax')->displayCartBothPrices()) echo 'col-total-excl'; else echo 'col-total'; ?> a-right">
    
        <span class="cell-label">
            <?php echo $this->__('Subtotal') ?>
            <?php if ($this->helper('tax')->displayCartBothPrices()): ?>
                <?php echo $this->helper('tax')->getIncExcTaxLabel(false) ?>
            <?php endif; ?>
        </span>
    
        <?php if (Mage::helper('weee')->typeOfDisplay($_item, array(1, 4), 'sales') && $_item->getWeeeTaxAppliedAmount()): ?>
            <span class="cart-tax-total" onclick="taxToggle('esubtotal-item-tax-details<?php echo $_item->getId(); ?>', this, 'cart-tax-total-expanded');">
        <?php else: ?>
            <span class="cart-price">
        <?php endif; ?>

            <?php if ($canApplyMsrp): ?>
                <span class="cart-msrp-subtotal">--</span>
            <?php else: ?>
                <?php if (Mage::helper('weee')->typeOfDisplay($_item, array(0, 1, 4), 'sales') && $_item->getWeeeTaxAppliedAmount()): ?>
                    <?php echo $this->helper('checkout')->formatPrice($_item->getRowTotal()+$_item->getWeeeTaxAppliedRowAmount()+$_item->getWeeeTaxRowDisposition()); ?>
                <?php else: ?>
                    <?php echo $this->helper('checkout')->formatPrice($_item->getRowTotal()) ?>
                <?php endif; ?>
            <?php endif; ?>

        </span>
        <?php if (Mage::helper('weee')->getApplied($_item)): ?>

            <div class="cart-tax-info" id="esubtotal-item-tax-details<?php echo $_item->getId(); ?>" style="display:none;">
                <?php if (Mage::helper('weee')->typeOfDisplay($_item, 1, 'sales') && $_item->getWeeeTaxAppliedAmount()): ?>
                    <?php foreach (Mage::helper('weee')->getApplied($_item) as $tax): ?>
                        <span class="weee"><?php echo $tax['title']; ?>: <?php echo Mage::helper('checkout')->formatPrice($tax['row_amount'],true,true); ?></span>
                    <?php endforeach; ?>
                <?php elseif (Mage::helper('weee')->typeOfDisplay($_item, 2, 'sales') && $_item->getWeeeTaxAppliedAmount()): ?>
                    <?php foreach (Mage::helper('weee')->getApplied($_item) as $tax): ?>
                        <span class="weee"><?php echo $tax['title']; ?>: <?php echo Mage::helper('checkout')->formatPrice($tax['row_amount'],true,true); ?></span>
                    <?php endforeach; ?>
                <?php elseif (Mage::helper('weee')->typeOfDisplay($_item, 4, 'sales') && $_item->getWeeeTaxAppliedAmount()): ?>
                    <?php foreach (Mage::helper('weee')->getApplied($_item) as $tax): ?>
                        <span class="weee"><?php echo $tax['title']; ?>: <?php echo Mage::helper('checkout')->formatPrice($tax['row_amount'],true,true); ?></span>
                    <?php endforeach; ?>
                <?php endif; ?>
            </div>

            <?php if (Mage::helper('weee')->typeOfDisplay($_item, 2, 'sales') && $_item->getWeeeTaxAppliedAmount()): ?>
                <div class="cart-tax-total" onclick="taxToggle('esubtotal-item-tax-details<?php echo $_item->getId(); ?>', this, 'cart-tax-total-expanded');">
                    <span class="weee"><?php echo Mage::helper('weee')->__('Total'); ?>: <?php echo $this->helper('checkout')->formatPrice($_item->getRowTotal()+$_item->getWeeeTaxAppliedRowAmount()+$_item->getWeeeTaxRowDisposition()); ?></span>
                </div>
            <?php endif; ?>
        <?php endif; ?>
    </td>
    <?php endif; ?>
    <?php if (($this->helper('tax')->displayCartPriceInclTax() || $this->helper('tax')->displayCartBothPrices()) && !$_item->getNoSubtotal()): ?>
    <td class="<?php if($this->helper('tax')->displayCartBothPrices()) echo 'col-total-incl'; else echo 'col-total'; ?>">
    
        <span class="cell-label">
            <?php echo $this->__('Subtotal') ?>
            <?php if ($this->helper('tax')->displayCartBothPrices()): ?>
                <?php echo $this->helper('tax')->getIncExcTaxLabel(true) ?>
            <?php endif; ?>
        </span>
    
        <?php $_incl = $this->helper('checkout')->getSubtotalInclTax($_item); ?>
        <?php if (Mage::helper('weee')->typeOfDisplay($_item, array(1, 4), 'sales') && $_item->getWeeeTaxAppliedAmount()): ?>
            <span class="cart-tax-total" onclick="taxToggle('subtotal-item-tax-details<?php echo $_item->getId(); ?>', this, 'cart-tax-total-expanded');">
        <?php else: ?>
            <span class="cart-price">
        <?php endif; ?>

            <?php if ($canApplyMsrp): ?>
                <span class="cart-msrp-subtotal">--</span>
            <?php else: ?>
                <?php if (Mage::helper('weee')->typeOfDisplay($_item, array(0, 1, 4), 'sales') && $_item->getWeeeTaxAppliedAmount()): ?>
                    <?php echo $this->helper('checkout')->formatPrice($_incl + Mage::helper('weee')->getRowWeeeTaxInclTax($_item)); ?>
                <?php else: ?>
                    <?php echo $this->helper('checkout')->formatPrice($_incl-$_item->getWeeeTaxRowDisposition()) ?>
                <?php endif; ?>
            <?php endif; ?>

        </span>


        <?php if (Mage::helper('weee')->getApplied($_item)): ?>

            <div class="cart-tax-info" id="subtotal-item-tax-details<?php echo $_item->getId(); ?>" style="display:none;">
                <?php if (Mage::helper('weee')->typeOfDisplay($_item, 1, 'sales') && $_item->getWeeeTaxAppliedAmount()): ?>
                    <?php foreach (Mage::helper('weee')->getApplied($_item) as $tax): ?>
                        <span class="weee"><?php echo $tax['title']; ?>: <?php echo Mage::helper('checkout')->formatPrice($tax['row_amount_incl_tax'],true,true); ?></span>
                    <?php endforeach; ?>
                <?php elseif (Mage::helper('weee')->typeOfDisplay($_item, 2, 'sales') && $_item->getWeeeTaxAppliedAmount()): ?>
                    <?php foreach (Mage::helper('weee')->getApplied($_item) as $tax): ?>
                        <span class="weee"><?php echo $tax['title']; ?>: <?php echo Mage::helper('checkout')->formatPrice($tax['row_amount_incl_tax'],true,true); ?></span>
                    <?php endforeach; ?>
                <?php elseif (Mage::helper('weee')->typeOfDisplay($_item, 4, 'sales') && $_item->getWeeeTaxAppliedAmount()): ?>
                    <?php foreach (Mage::helper('weee')->getApplied($_item) as $tax): ?>
                        <span class="weee"><?php echo $tax['title']; ?>: <?php echo Mage::helper('checkout')->formatPrice($tax['row_amount_incl_tax'],true,true); ?></span>
                    <?php endforeach; ?>
                <?php endif; ?>
            </div>

            <?php if (Mage::helper('weee')->typeOfDisplay($_item, 2, 'sales') && $_item->getWeeeTaxAppliedAmount()): ?>
                <div class="cart-tax-total" onclick="taxToggle('subtotal-item-tax-details<?php echo $_item->getId(); ?>', this, 'cart-tax-total-expanded');">
                    <span class="weee"><?php echo Mage::helper('weee')->__('Total incl. tax'); ?>: <?php echo $this->helper('checkout')->formatPrice($_incl + Mage::helper('weee')->getRowWeeeTaxInclTax($_item)); ?></span>
                </div>
            <?php endif; ?>
        <?php endif; ?>
    </td>
    <?php endif; ?>
    <td class="col-delete a-center"><a href="<?php echo $this->getDeleteUrl()?>" title="<?php echo Mage::helper('core')->quoteEscape($this->__('Remove item')) ?>" class="btn-remove btn-remove2"><?php echo $this->__('Remove item')?></a></td>
</tr>

```

# ADD TO CART

/Volumes/dev.wheelstandcentre.nl/domains/dev.wheelstandcentre.nl/public_html/app/design/frontend/ultimo/mardomedia/template/catalog/product/view/addtocart.phtml

```
<?php
/**
 * Magento
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Academic Free License (AFL 3.0)
 * that is bundled with this package in the file LICENSE_AFL.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/afl-3.0.php
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to license@magentocommerce.com so we can send you a copy immediately.
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade Magento to newer
 * versions in the future. If you wish to customize Magento for your
 * needs please refer to http://www.magentocommerce.com for more information.
 *
 * @category    design
 * @package     base_default
 * @copyright   Copyright (c) 2012 Magento Inc. (http://www.magentocommerce.com)
 * @license     http://opensource.org/licenses/afl-3.0.php  Academic Free License (AFL 3.0)
 */
?>
<?php $_product = $this->getProduct(); ?>
<?php $buttonTitle = $this->__('Add to Cart'); ?>
<?php $inventory =  Mage::getModel('cataloginventory/stock_item')->loadByProduct($_product); ?>
<!-- Display Op Bestelling and Change Cart button ICTWebSolution -->
<?php $attributeValue = $_product->getAttributeText('op_bestelling'); ?>
<?php $staticBlockIdentifier = $attributeValue; ?>
<?php
/** also add: 
	<?php if ($attributeValue == op_bestelling_yes){echo 'op-bestelling';}?> to the class
<!-- END Display Op Bestelling and Change Cart button ICTWebSolution -->
*/
?>
<?php if ($_product->isSaleable()): ?>
    <div class="add-to-cart left-side">
      <button type="button" title="<?php echo $buttonTitle ?>" id="product-addtocart-button" class="button btn-cart <?php if ($attributeValue == op_bestelling_yes){echo 'op-bestelling';}?> <?php if ($inventory->getBackorders() == '101'){ echo 'pre-order';}?>" onclick="productAddToCartForm.submit(this)">
        <span class="buy-product-view">
          <!--<span><?php echo $buttonTitle ?></span>-->
        <!-- Display Op Bestelling and Change Cart button ICTWebSolution -->
        <span>
          	<?php if ($attributeValue == op_bestelling_yes):?><?php echo $this->__('Op Bestelling') ?>
          	<?php elseif ($attributeValue = op_bestelling_no):{ echo $buttonTitle;} ?>
  			<?php endif;?>
  		</span>
  		<!-- END Display Op Bestelling and Change Cart button ICTWebSolution -->
          <i class="fa fa-size fa-shopping-cart"></i>
        </span>
      </button>
    </div>
    <div class="paypal-wrapper"><?php echo $this->getChildHtml('', true, true) ?></div>
<?php endif; ?>
<!-- Display Op Bestelling and Change Cart button ICTWebSolution -->
<?php if ($attributeValue == op_bestelling_yes):?>
	<?php echo $this->getLayout()->createBlock('cms/block')->setBlockId('op_bestelling_yes')->toHtml();?>
<?php endif; ?>
<!-- END Display Op Bestelling and Change Cart button ICTWebSolution -->
```
