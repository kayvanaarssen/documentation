Start by adding to following to this file:

`domains/wheelstandcentre.nl/public_html/app/design/frontend/ultimo/mardomedia/template/catalog/product/view.phtml`

# Breakdown of code:

Add the custom php functions to the file:

```
<?php $_product = $this->getProduct();?>
<?php $_geschiktvoorgame = $_product->getData('geschikt_voor_game'); ?>
```

Add the code to display the attribute on the page:

```
<!-- Display Compatible with Game ICTWebsolution --> 
	<div class="compatible-with-game">
		<?php if ($_geschiktvoorgame): ?>
		<h3>
			<?php echo $this->helper('catalog/output')->productAttribute($this->getProduct(), $_name, 'name') ?> <span><?php echo $this->__('is geschikt voor') ?>:</span>
		</h3>
		
		<?php echo $this->helper('catalog/output')->productAttribute($this->getProduct(), $_geschiktvoorgame, 'geschikt_voor_game') ?>
		<?php endif; ?>
	</div>
		<!-- Add Read More / Less Option -->
		<div class="fade_out" style="backround-color: rgba(255,255,255,0.5);">
			<?php $readmoregame = $this->__('Read more'); ?>
			<?php $readlessgame = $this->__('Read less'); ?>
			<div class="read-more-game"><?php echo $readmoregame; ?></div>
		</div>
		<!-- END Add Read More / Less Option -->
    <!-- END Display Compatible with Game ICTWebsolution --> 
```

Also add the needed JS Code:

```
<!-- Read More/Less for Capatible With Game ICTWebsolution --> 
<script>
	jQuery(".read-more-game").click(function () {
        if(jQuery(".compatible-with-game").hasClass("compatible-game-long")) {
            jQuery(this).text("<?php echo $readmoregame; ?>");
        } else {
            jQuery(this).text("<?php echo $readlessgame; ?>");
        }

        jQuery(".compatible-with-game").toggleClass("compatible-game-long");
    });
</script>
<!-- END Read More/Less for Capatible With Game ICTWebsolution --> 
```


# Complete Code for the page:

```
<?php
/**
 * Magento
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Academic Free License (AFL 3.0)
 * that is bundled with this package in the file LICENSE_AFL.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/afl-3.0.php
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to license@magento.com so we can send you a copy immediately.
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade Magento to newer
 * versions in the future. If you wish to customize Magento for your
 * needs please refer to http://www.magento.com for more information.
 *
 * @category    design
 * @package     base_default
 * @copyright   Copyright (c) 2006-2015 X.commerce, Inc. (http://www.magento.com)
 * @license     http://opensource.org/licenses/afl-3.0.php  Academic Free License (AFL 3.0)
 */

/**
 * Product description template
 *
 * @see Mage_Catalog_Block_Product_View_Description
 */
?>
<?php $_description = $this->getProduct()->getDescription(); ?>
<?php $_name = $this->getProduct()->getName(); ?>
<?php $_odd = true; ?>
<?php $_attributes = array('merk', 'model', 'platform', 'artikelnummer', 'geschikt_voor'); ?>
<?php $_product = $this->getProduct();?>
<?php $_geschiktvoorgame = $_product->getData('geschikt_voor_game'); ?>


<div class="grid12-12" id="product-description-right">

		<?php if ($_description): ?>
	    	<h2><?php echo $this->__('Description') ?> <?php echo $this->helper('catalog/output')->productAttribute($this->getProduct(), $_name, 'name') ?> </h2>
			    <div class="std">
			        <?php echo $this->helper('catalog/output')->productAttribute($this->getProduct(), $_description, 'description') ?>
			    </div>
		
			<div class="fade_out" style="backround-color: rgba(255,255,255,0.5);">
				<?php endif; ?>
				<?php $readmore = $this->__('Read more'); ?>
				<?php $readless = $this->__('Read less'); ?>
				<div class="read-more"><?php echo $readmore; ?></div>
				<div class="clear-line"></div>
			</div>

	<!-- Display Compatible with Game ICTWebsolution --> 
	<div class="compatible-with-game">
		<?php if ($_geschiktvoorgame): ?>
		<h3>
			<?php echo $this->helper('catalog/output')->productAttribute($this->getProduct(), $_name, 'name') ?> <span><?php echo $this->__('is geschikt voor') ?>:</span>
		</h3>
		
		<?php echo $this->helper('catalog/output')->productAttribute($this->getProduct(), $_geschiktvoorgame, 'geschikt_voor_game') ?>
		<?php endif; ?>
	</div>
		<!-- Add Read More / Less Option -->
		<div class="fade_out" style="backround-color: rgba(255,255,255,0.5);">
			<?php $readmoregame = $this->__('Read more'); ?>
			<?php $readlessgame = $this->__('Read less'); ?>
			<div class="read-more-game"><?php echo $readmoregame; ?></div>
		</div>
		<!-- END Add Read More / Less Option -->
    <!-- END Display Compatible with Game ICTWebsolution --> 
</div>
    <!-- Read More/Less for Capatible With Game ICTWebsolution --> 
<script>
	jQuery(".read-more-game").click(function () {
        if(jQuery(".compatible-with-game").hasClass("compatible-game-long")) {
            jQuery(this).text("<?php echo $readmoregame; ?>");
        } else {
            jQuery(this).text("<?php echo $readlessgame; ?>");
        }

        jQuery(".compatible-with-game").toggleClass("compatible-game-long");
    });
</script>
<!-- END Read More/Less for Capatible With Game ICTWebsolution --> 

<script>
	jQuery(".read-more").click(function () {
        if(jQuery(".std").hasClass("product-description-long")) {
            jQuery(this).text("<?php echo $readmore; ?>");
        } else {
            jQuery(this).text("<?php echo $readless; ?>");
        }

        jQuery(".std").toggleClass("product-description-long");
    });
</script>
```
