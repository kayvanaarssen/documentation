MardoMedia - Payment Logos Footer

Add this to the `footer.phtml` in the folder: `domains/wheelstandcentre.nl/public_html/app/design/frontend/ultimo/mardomedia/template/page`


```
<!-- Footer Payment Logos -->
	<div class="footer-payment-logos">
	<div class="footer-primary-payment-logos footer container">
				<?php echo $this->getLayout()->createBlock('cms/block')->setBlockId('footer-payment-logos')->toHtml();?>
			</div>
	
	</div>
<!-- END Footer Payment Logos -->
```

## Create a static block in the backend Called: `footer-payment-logos`

The code for the Static block is: 

```
    <div class="logos-footer">
        <ul>
            <li class="trust-logo">
                <img alt="PayPal" title="PayPal" src="http://www.mardomedia.nl/media/payment-logos/Payment_Paypal.png">
            </li>
            <li class="trust-logo">
                <img alt="MasterCard" title="MasterCard" src="http://www.mardomedia.nl/media/payment-logos/Payment_MasterCard.png">
            </li>
            <li class="trust-logo">
                <img alt="Visa" title="Visa" src="http://www.mardomedia.nl/media/payment-logos/Payment_Visa.png">
            </li>
            <li class="trust-logo">
                <img alt="Klarna" title="Klarna" src="http://www.mardomedia.nl/media/payment-logos/Payment_Klarna.png">
            </li>
            <li class="trust-logo">
                <img alt="DPD" title="DPD" src="http://www.mardomedia.nl/media/payment-logos/Payment_DPD.png">
            </li>
        </ul>
    </div>
```

# CSS Code:

```
/* Added on 28-10-2016 */


.footer-payment-logos{
	border-top: 1px solid #efefef;
	background-color: #fff;
	.footer-primary-container, .section-container{
		background-color: : #ffffff;
	}
	.footer-primary-payment-logos{
	background-color: #fff;
    color: #000;
    padding-top: 5px;
    padding-bottom: 10px;
    /*width: 1100px;
    margin-left: auto;
    margin-right: auto; */
	}
	.logos-footer{
		float: none;
		margin: 0;
		padding: 0 1.5rem;
		text-align: center;
		.trust-logo{
			img{
				max-height: 35px;
			}
			display: inline-block;
			margin-right: 5px;
			margin-top: 5px;
		}

	}
}
```

