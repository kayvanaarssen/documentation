```
title Install the Synergy Office Integration on a Client or Server
@echo off

echo.
echo Please enter the Synergy Enterprise-url below and press enter
echo (include http:// or https://)
set synurl="http://exact/synergy"
{ENTER}
cls
goto pmachine

:pmachine
set allusers=1
set appfold=EXACTWIXAPPFOLDER="exactWixPerMachineFolder"
goto execute

:execute
cls
echo Installing Exact Synergy Enterprise Office Integration...
echo Please Wait... ...
msiexec /i "\\exact\D$\Exact\SOI\SynergyOfficeIntegration.msi" %appfold% SYNERGYURL=%synurl% FIRSTRUN=0 SETPREREQS=1 USEROAMINGFOLDER=1 ALLUSERS=%allusers% /quiet /l*v "\\exact\D$\Exact\SOI\SOI Installation.log"
goto end
:end
cls
```