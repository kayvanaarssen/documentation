Install the needed packages:

`yum install net-snmp-utils -y`

`yum install net-snmp-utils net-snmp -y`

Make a backup of the default config and edit the config files:

```
mv /etc/snmp/snmpd.conf /etc/snmp/snmpd.conf.org
nano /etc/snmp/snmpd.conf
```

Add the following to the file:

```
rocommunity  srv99
syslocation  "Amsterdam, The Netherlands"
syscontact  Kay van Aarssen
```

Start the SNMP Service and add it to the Startup group:

```
/etc/init.d/snmpd start
chkconfig snmpd on
```