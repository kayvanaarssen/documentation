Owncloud - SMB Mount as Default

Add this to the `/var/www/data/kay/mount.json` file

```
{
    "user": {
        "kay": {
            "\/kay\/files\/": {
                "id": 1,
                "backend": "smb",
                "authMechanism": "password::password",
                "options": {
                    "host": "10.200.0.5",
                    "share": "owncloud",
                    "root": "",
                    "domain": "",
                    "user": "owncloud",
                    "password": "",
                    "password_encrypted": "dmoyMTNkenZkNmxidmNndA+gLv6g7gz08tK+T1tv53oI0MoKCQ10g3KJ\/LTYbBeO"
                },
                "mountOptions": {
                    "encrypt": true,
                    "previews": true,
                    "filesystem_check_changes": 1
                }
            }
        }
    }
}
```