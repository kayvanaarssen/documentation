## Install Dependencies:

`yum install nfs-utils nfs-utils-lib`

`mkdir /mnt/NFS`

## Add the script to automount

`nano /root/start.sh`

Add: 

```
/bin/sleep 15
/etc/init.d/rpcbind restart
/bin/sleep 5
/bin/mount 10.200.0.5:/volume1/R1Soft-Backups /mnt/r1soft -o nolock
```

## Give the Script execute rights:

`chmod +x /root/start.sh'

## Add the script to the startup cron:

`nano /etc/cron.d/startup`

Add:

`@reboot root /root/start.sh`

# Extra information:

Setting Up the NFS Client
Step One—Download the Required Software

Start off by using apt-get to install the nfs programs.

`yum install nfs-utils nfs-utils-lib`

Step Two—Mount the Directories

Once the programs have been downloaded to the the client server, create the directory that will contain the NFS shared files

`mkdir -p /mnt/nfs/home`

Then go ahead and mount it

`mount 12.34.56.789:/home /mnt/nfs/home`

You can use the df -h command to check that the directory has been mounted. You will see it last on the list.

```
df -h
Filesystem            Size  Used Avail Use% Mounted on
/dev/sda               20G  783M   18G   5% /
12.34.56.789:/home       20G  785M   18G   5% /mnt/nfs/home
```

Additionally, use the mount command to see the entire list of mounted file systems.

`mount`

Your list should look something like this:

```
/dev/sda on / type ext4 (rw,errors=remount-ro)
none on /proc/sys/fs/binfmt_misc type binfmt_misc (rw)
sunrpc on /var/lib/nfs/rpc_pipefs type rpc_pipefs (rw)
nfsd on /proc/fs/nfsd type nfsd (rw)
12.34.56.789:/home on /mnt/nfs/home type nfs (rw,noatime,nolock,bg,nfsvers=2,intr,tcp,actimeo=1800,addr=12.34.56.789)
``