>R1Soft does not at this time have a way of obtaining its drivers for the current Kernel version using the standard r1soft-setup –get-module command. To install the driver on CentOS7 follow these steps:

# Locate current kernel version
   
`uname -r`

```
3.10.0-229.11.1.el7.x86_64
```

## Visit http://repo.r1soft.com/modules/Centos_7_x64/ and search for hcpdriver matching kernel version.

Place the hcpdriver found above in the directory:

```
cd /lib/modules/r1soft
wget "Your Module version here"
```

Restart the agent

`/etc/init.d/cdp-agent restart`

Verify the driver is now loaded

`lsmod | grep hcp`

```
hcpdriver 81014 4
```

This will install the driver and allow you to continue with setting up the r1soft backup agent.