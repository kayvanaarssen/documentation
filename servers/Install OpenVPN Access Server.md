First install Nano en Wget

`yum install -y nano wget`

Then download the Open VPN Access Server

`wget http://swupdate.openvpn.org/as/openvpn-as-2.0.4-CentOS6.x86_64.rpm`

Then install the Open VPN Access Server

`rpm -i http://swupdate.openvpn.org/as/openvpn-as-2.0.4-CentOS6.x86_64.rpm`

Then change the OpenVPN user password:

`passwd openvpn`


Note: In some circumstances for certain configurations you may need to run the complete ovpn-init script terminal: 

`/usr/local/openvpn_as/bin/ovpn-init`

Configuring the Admin Web Interface:
After you have completed the Initial Configuration Tool you should then be able to access the Admin Web Interface through your preferred web browser. You should have noticed an link to the Admin Web Interface after you completed the Initial Configuration Tool, if you missed it you can access the Admin Web Interface by typing the following in your browsers address bar: https://openvpnasip:943/admin (Please replace "openvpnasip" with the IP you allocated to your openvpn-as instance)

You can now go ahead and login with your openvpn admin credentials. Once logged in you will see the following screen: