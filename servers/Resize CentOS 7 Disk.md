https://www.youtube.com/watch?v=YuED2n8_SHE

1. I made the partition type as Standard and let Centos create them Automatically.. / partition was /dev/sda3 and with 8.71GB Format Option was xfs

2. After running "parted" it shows my existing disk size is 10.7GB .. Lets increase it up to 15Gb and give a restart

3. Run a "Parted" again and check new disk size. now it shows 16.1GB

4. Now we are going to increase size of / partition (/dev/sda3). Please note that to do this the partition you are going to increase should be the last partition on partition table .

5. First we have to delete the existing / partition entry from Partition table . Dont worry this will not delete your data .

6. Run fdisk /dev/sda

7. Type "p" to print partition table . Check if the /dev/sda3 drive have bootable flag (*) and make a note of the starting cylinder. When creating the new parttion with increased size the starting cylinder of the new partition should be the same as the old one 

8. Type "d" and select partition that you are going to delete . ( the last one)

9. Now create a new one . Make the partition type as primary . Extend will destroy everything

8. Give the starting cylinder as same as the old one . If your are going to use the whole available space for this partion just type return for the end cylinder . it will allocate all available space to this new partition

9. Now type "w" and write these changes to the partition table . 

10. Check blkid of the new partition and make changes to fstab if necessary

11. Now go ahead and restart your computer . 

12. This partition will not be resized until we run resize2fs or xfs_growfs . resize2fs only supports ext file format partitions . This is a xfs partition so we have to use xfx_growfs tool .

13. First run xfs_info /dev/sda3 to check current details of the partition .

14. Then run xfs_growfs /dev/sda3 this will do on-line resize of our / partion . Thats it .