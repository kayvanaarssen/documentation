# Adding the Server Key to Linux Agent

`r1soft-setup --get-key [Server_URL]`

# Installing Agent on CentOS, RHE and Fedora

1. Installing Agent Using YUM

1.1 Configure YUM Repository

>YUM is the easiest way to keep programs up-to-date on RedHat-compatible distributions. It downloads and installs the latest version of a program. You should configure the YUM repository to manage install and upgrades of CDP Agent

>First, create a YUM .repo file with the R1Soft repository information. Save the file in the yum.repos.d directory which is typically located in /etc/.

1. Open the new file with a text editor such as vi or nano:

```
cd /etc/yum.repos.d
vi r1soft.repo 
```

or

`nano -w /etc/yum.repos.d/r1soft.repo`

2. Insert the following text into the file and save the file:

```
[r1soft]
name=R1Soft Repository Server
baseurl=http://repo.r1soft.com/yum/stable/$basearch/
enabled=1
gpgcheck=0 
```

3. To verify what is written to the file, use the following command:

`cat /etc/yum/yum.repos.d/r1soft.repo`

1.2 Install the Package

Once you have configured the YUM repository, you can use the following command to install CDP Agent:

`yum install r1soft-cdp-enterprise-agent`
