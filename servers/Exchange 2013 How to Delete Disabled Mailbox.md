Exchange 2013 How to Delete Disabled Mailbox

First Search fot the disabled mailboxes

> Change the Database name if needed

`Get-MailboxStatistics –Database "CWC-DB03" | where {$_.disconnectdate –ne $null} | select displayname,MailboxGUID`

You now get a list with the outputs and Mailbox Identity's now select the identity

```
DisplayName                                                 MailboxGuid
-----------                                                 -----------
Mailbox1                                   		            4595c066-c17d-470a-97dd-755f3de89b3a
Mailbox2                                            	    5770b2bf-9350-4c13-b286-d2821b7ff0d2
```

Execute the command with the needed Identity

`Remove-Mailbox –Database "CWC-DB04" –StoreMailboxIdentity 9813aa76-a5f2-4a58-976c-32bc7128ca92`