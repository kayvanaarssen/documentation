# Task: Disable / Turn off Linux Firewall (Red hat/CentOS/Fedora Core)

Type the following two commands (you must login as the root user):

```
/etc/init.d/iptables save
/etc/init.d/iptables stop
```

Turn off firewall on boot:

`chkconfig iptables off`

## Task: Enable / Turn on Linux Firewall (Red hat/CentOS/Fedora Core)

Type the following command to turn on iptables firewall:

`/etc/init.d/iptables start`

Turn on firewall on boot:

`chkconfig iptables on`

```
/etc/init.d/iptables save
/etc/init.d/iptables stop
chkconfig iptables off
```
