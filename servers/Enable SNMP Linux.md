First install the basic SNMP Tools:

`yum install net-snmp-utils -y`

OR

`yum install net-snmp-utils net-snmp -y`

Move the default SNMP Config to a Backup

```
mv /etc/snmp/snmpd.conf /etc/snmp/snmpd.conf.org
nano /etc/snmp/snmpd.conf
```

Add the following to the `/etc/snmp/snmp.conf`

```
rocommunity  srv99
syslocation  "Amsterdam, The Netherlands"
syscontact  Kay van Aarssen
```

Now start the SNMP Demon and add it to the startup list:

```
/etc/init.d/snmpd start
chkconfig snmpd on
```