# Original URL: https://www.linode.com/docs/websites/lamp/lamp-server-on-debian-7-wheezy

# Apache

# Install and Configure

## Install Apache 2.2:

`sudo apt-get install apache2`

Edit the main Apache configuration file to adjust the resource use settings. The settings shown below are a good starting point for a Linode 1GB:

`/etc/apache2/apache2.conf`

```
KeepAlive Off

<IfModule mpm_prefork_module>
StartServers 2
MinSpareServers 6
MaxSpareServers 12
MaxClients 30
MaxRequestsPerChild 3000
</IfModule>
``` 

>Configure Name-Based Virtual Hosts
>There are different ways to set up virtual hosts; however, the method below is recommended.

>Within the `/etc/apache2/sites-available/` directory, create a configuration file for your website, `example.com.conf`, replacing example.com with your own domain information:

`/etc/apache2/sites-available/example.com.conf`

```
<VirtualHost *:80> 
     ServerAdmin webmaster@example.com
     ServerName example.com
     ServerAlias www.example.com
     DocumentRoot /var/www/example.com/public_html/
     ErrorLog /var/www/example.com/logs/error.log 
     CustomLog /var/www/example.com/logs/access.log combined
</VirtualHost>
```

>The ErrorLog and CustomLog entries are suggested for more fine-grained logging, but are not required. If they are defined (as shown above), the logs directories must be created before you restart Apache.

### Create the above-referenced directories:

```
sudo mkdir -p /var/www/example.com/public_html
sudo mkdir /var/www/example.com/logs
```

Enable the website’s virtual host:

`sudo a2ensite example.com.conf`

If you need to disable your website later, run:

`sudo a2dissite example.com.conf`

Restart Apache:

`sudo service apache2 restart`

Assuming that you have configured the DNS for your domain to point to your Linode’s IP address, virtual hosting for your domain should now work.

## MySQL

### Install and Configure

### Install MySQL:

`sudo apt-get install mysql-server`

> Choose a secure password when prompted.

Run `mysql_secure_installation` , a program that helps secure MySQL. You will be presented with the opportunity to change the MySQL root password, remove anonymous user accounts, disable root logins outside of localhost, and remove test databases:

`mysql_secure_installation`

>Create a MySQL Database
>Log into MySQL:

`mysql -u root -p`

>Enter the root password. The MySQL prompt will appear.
>Create a database and a user with permissions for it. In this example the databse is called webdata, the user webuser and password password:

```
create database webdata; 
grant all on webdata.* to 'webuser' identified by 'password';
```

Exit MySQL:

`quit`

With Apache and MySQL installed, you are now ready to move on to installing PHP.

# PHP

## Install PHP, and the PHP Extension and Application Repository:

`sudo apt-get install php5 php-pear`

If you need MySQL support also install `php5-mysql`

`sudo apt-get install php5-mysql`

Once PHP5 is installed, tune the configuration file located in `/etc/php5/apache2/php.ini` to enable more descriptive errors, logging, and better performance. The following modifications provide a good starting point:

`/etc/php5/apache2/php.ini`

```
error_reporting = E_COMPILE_ERROR|E_RECOVERABLE_ERROR|E_ERROR|E_CORE_ERROR
error_log = /var/log/php/error.log  
max_input_time = 30
```

>Ensure the lines above are uncommented. Commented lines begin with a semicolon (;).
>Create the log directory for PHP and give the Apache user ownership:

```
sudo mkdir /var/log/php
sudo chown www-data /var/log/php
```

Restart Apache:

`sudo service apache2 restart`

Congratulations! You have now set up and configured a LAMP stack.