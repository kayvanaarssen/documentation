# Install Unifi Video on a Unifi NVR when AirVideo is installed

```
sudo -i
apt-get clean
cd /var/lib/apt
mv lists lists.old
mkdir -p lists/partial
apt-get clean
apt-get update
exit
```

`wget http://dl.ubnt.com/firmwares/unifi-video/3.1.1/unifi-video_3.1.1~Ubuntu14.04_amd64.deb`

`sudo dpkg -i unifi-video_3.1.1~Ubuntu14.04_amd64.deb; sudo apt-get install -f`