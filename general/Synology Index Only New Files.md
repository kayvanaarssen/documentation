To my knowledge, no DiskStation model currently exists where the kernel supports inotify. This represents a huge problem for people who have custom scripts to add multimedia to your DiskStations. While the DiskStations media indexer supports automatic indexing of files being added via SMB, AFP or FTP, it is not aware if you move or copy files via SSH/Telnet on the DiskStation directly.

I have therefore written the Synology Media Indexer - http://www.naschenweng.info/media/blogs/a/software/downloads/syno-media-indexer.zip which is a simple perl-script which scans your video directory and adds new video-files to the indexer. The indexing is lighting fast as only new files get added (compared to the full reindex of all media if you use the “Reindex”-function in the admin interface).

The script will scan the video directory for modified files over the last two days and will then query the synoindex-service if the file was already indexed. If the file does not exist in the index database, the script will manually add it for immediate indexing.

I have included my most common media types in the script, but if I missed something, you are welcome to extend the script (and let me know what types I have missed). The script itself is easy to read, and I suggest you go through the code as there might be minor tweaks you want to make.

Download: http://www.knowledgebase.ictwebsolution.nl/wp-content/uploads/2014/04/syno-media-indexer.zip