Als je alleen “define( ‘WP_AUTO_UPDATE_CORE’, false );” aan je wp-config.php bestanden wilt toevoegen (aangezien het er nog niet staat), kun je daar ook volgende scriptje voor gebruiken: Iets makkelijker en het voegt de ‘define’ aan alle wp-config.php bestanden toe.

```
#!/bin/sh

updatedb
for x in `locate wp-config.php`
do
echo “define( ‘WP_AUTO_UPDATE_CORE’, false );” >> ${x}
done
```

Opslaan als tekst bestand zoals ‘wp-ding.sh’

`chmod u+x wp-ding.sh`

en dan uitvoeren met:

`./wp-ding.sh`

Lees meer: 
Re: WordPress autoupdate disable – webhostingtalk.nl http://www.webhostingtalk.nl/private.php?do=showpm&pmid=617139#ixzz2jrPPoflK

```
#!/bin/sh

# bijwerken mlocate index
echo -n “Updating mlocate index…”
updatedb
echo ” done”
#
# ‘add’ moet de exacte setting bevatten die je wilt toevoegen
#
add=”define (‘WP_AUTO_UPDATE_CORE’, false);”;
#
# ‘check’ moet de exacte tekst bevatten waar je op wilt controleren (zodat de setting, indien reeds aanwezig, niet overschreven worden).
#
chk=”WP_AUTO_UPDATE_CORE”;
for x in `locate wp-config.php`
do
ret=`grep “${chk}” ${x}`
if [ “x$ret” == “x” ]; then
echo “Adding setting to: ${x}”
echo “${add}” >> $x
else
echo “Setting already exists in: ${x}”
fi
done
```
