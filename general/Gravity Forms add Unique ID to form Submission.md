# CREATE UNIVERSALLY UNIQUE IDENTIFIER (UUID) WITH GRAVITY FORMS

**Please note an updated post has been posted here**

**Below code has been updated as of 4/26/2016 to fix a formatting issue caused by WordPress**

>> First & foremost this is my first rodeo with Gravity Forms. Yes, I know, I’m very late to the game. In the past I’ve either built custom forms or more recently, started using Ninja Forms. Ninja Forms is a pretty slick form builder & I love a lot of the out-of-the-box features. However, when it came to Payment Processing it fell short. We decided to go with Gravity Forms.

My client wanted to create a UUID every time the form was filled out and then send that ID to the user as a reference number of sorts.  I’ll describe the steps below.

>> 1. Copy & Paste the below code into your functions.php file. You’ll need to edit lines 6, 21, & 22. Line 6 will create a alpha prefix that goes in front of your unique 8 digit number, you can put whatever prefix you want here. Line 21 should be replaced with the form ID of the form you want to create a UUID on. Line 22 should be replaced with the hidden field ID you create on your form.

```
/* -------------------------------------------------------------------------------------
UUID for forms
----------------------------------------------------------------------------------- */
add_filter("gform_field_value_uuid", "get_unique");

function get_unique(){

$prefix = "vbs15"; // update the prefix here

do {
$unique = mt_rand();
$unique = substr($unique, 0, 8);
$unique = $prefix . $unique;
} while (!check_unique($unique));

return $unique;
}

function check_unique($unique) {
global $wpdb;

$table = $wpdb->prefix . 'rg_lead_detail';
$form_id = 2; // update to the form ID your unique id field belongs to
$field_id = 122; // update to the field ID your unique id is being prepopulated in
$result = $wpdb->get_var("SELECT value FROM $table WHERE form_id = '$form_id' AND field_number = '$field_id' AND value = '$unique'");

if(empty($result))
return true;

return false;
}

```

2. Go into your form & create a hidden field.

3. Name the field whatever you want. I use “Group ID”

4. Click “Advanced” and check: “Allow field to be populated dynamically”

5. Enter: uuid in the parameter name

6. Save & Update the form. Confirm it’s working.

That’s it. Now you have your forms creating a UUID on submission. My UUID’s look like this: rhino74852136

If you have any questions shoot me a message.

Resources:

This post relies heavily on some code presented on the Gravity Forms Forum