# Export Magento Customer info form database

Create a file in the root directory of your magento installation called `export.php` or whatever you want to call it.

Put the following in the file:

```
<?php
require_once('app/Mage.php');
umask(0);
if (!Mage::isInstalled()) {
    echo "Application is not installed yet, please complete install wizard first.";
    exit;
}
// Only for urls // Don't remove this
$_SERVER['SCRIPT_NAME'] = str_replace(basename(__FILE__), 'index.php', $_SERVER['SCRIPT_NAME']);
$_SERVER['SCRIPT_FILENAME'] = str_replace(basename(__FILE__), 'index.php', $_SERVER['SCRIPT_FILENAME']);
Mage::app('admin')->setUseSessionInUrl(false);
Mage::setIsDeveloperMode(true); ini_set('display_errors', 1); error_reporting(E_ALL);
try {
    Mage::getConfig()->init();
    Mage::app();   
} catch (Exception $e) {
    Mage::printException($e);
}
ini_set('memory_limit','500M');
$customerCount = 0;
try{
    //configure the collection filters.
    $collection = Mage::getResourceModel('customer/customer_collection')
    ->addAttributeToSelect('firstname')
    ->addAttributeToSelect('lastname')
    ->addAttributeToSelect('email')
    ->addAttributeToSelect('store_id');
    //Add a page size to the result set.
    $collection->setPageSize(50);
    //discover how many page the result will be.
    $pages = $collection->getLastPageNumber();
    $currentPage = 1;
    //This is the file to append the output to.
    $fp = fopen('customers.csv', 'w');
    $addedKeys = false;
    do{
         //Tell the collection which page to load.
         $collection->setCurPage($currentPage);
         $collection->load();
         foreach ($collection as $customer){
            //write the collection array as a CSV.
            $customerArray = $customer->toArray();
            $customerREquiredArray['First name'] = $customerArray['firstname'];
            $customerREquiredArray['Last name'] = $customerArray['lastname'];
            $customerREquiredArray['Email'] = $customerArray['email'];
            $customerREquiredArray['Store ID'] = $customerArray['store_id'];
            if($addedKeys == false){
                $keys = array_keys($customerREquiredArray);
                fputcsv($fp, $keys);
                $addedKeys = true;
            }
            //var_dump($customerArray); echo "\n\n";
            fputcsv($fp, $customerREquiredArray);
            //fwrite($fp,print_r($customerArray,true) . chr(10) );
            $customerCount++;
         }
         $currentPage++;
         //make the collection unload the data in memory so it will pick up the next page when load() is called.
         $collection->clear();
         //break; //DEBUG
         echo "Finished page $currentPage of $pages \n"; 
    } while ($currentPage <= $pages);
    fclose($fp);
} catch (Exception $e) {
    //$response['error'] = $e->getMessage();
    Mage::printException($e);
}
echo "Saved $customerCount customers to csv file \n";
```

Next open your web browser and append the just created file.

After the file runs for a while if there are a lot of records. You will see a file calles: `customers.csv` in the root of your magento installation.