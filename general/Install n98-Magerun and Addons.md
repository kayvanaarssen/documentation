Install n98-Magerun and Addons

Download and Install Phar File

Download the latest stable N98-Magerun phar-file from the file-server:

`wget https://files.magerun.net/n98-magerun.phar`

Verify the download by comparing the MD5 checksum with the one on the website:

`md5sum n98-magerun.phar`

If it shows the same checksum as on the website, you downloaded the file successfully.

Now you can make the phar-file executable:

`chmod +x ./n98-magerun.phar`

The base-installation is now complete and you can verify it:

`./n98-magerun.phar --version`

If you want to use the command system wide you can copy it to /usr/local/bin.

`sudo cp ./n98-magerun.phar /usr/local/bin/`

### Fast command version:

```
wget https://files.magerun.net/n98-magerun.phar
md5sum n98-magerun.phar
chmod +x ./n98-magerun.phar
./n98-magerun.phar --version
sudo cp ./n98-magerun.phar /usr/local/bin/
```

# Update

Since version 1.1.0 we deliver a self-update script within the phar file:

`n98-magerun.phar self-update`

# Install ADD-On

## Make sure you have installed GIT if not install it:

`yum instal git -y`

Add Server key to Github:

`cat ~/.ssh/id_dsa.pub`

Add the public key to your github account.

```
mkdir -p ~/.n98-magerun/modules/
mkdir -p /usr/local/share/n98-magerun/modules
cd ~/.n98-magerun/modules/ && git clone git@github.com:peterjaap/magerun-addons.git pj-addons
cd /usr/local/share/n98-magerun/modules && git clone git@github.com:peterjaap/magerun-addons.git pj-addons
```

