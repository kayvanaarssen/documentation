Download de laatste versie van spotweb

https://github.com/spotweb/spotweb

Bewaar de volgende bestanden in een andere map:

```
settings.php
ownsettings.php
dbsettings.inc.php
```

Voer een update van de database uit:

`php ./update-db.php`

Voer hierna een Retrieve uit: 

`php ./retrieve.php`