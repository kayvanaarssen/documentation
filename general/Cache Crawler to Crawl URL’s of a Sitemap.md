# Gebruik onderstaand script om urls van een sitemap te crawlen en deze in de cache te zetten.

>Sla het script op onder een gewenste naam in de directory van de user. voorbeeld: cache.sh
>Voer het script daarna uit via SSH of door middel van een cronjob op de gewenste tijd.

## Script:

```
#!/bin/bash

## Define variables
ALIAS='@ddc.production'
URL='ictwebsolution.nl'
SITEMAP1='page-sitemap.xml'
SITEMAP2='portfolio-sitemap.xml'
SITEMAPS="$SITEMAP1 $SITEMAP2"

## Download every sitemap in SITEMAPS and save the output to 'tocrawl.txt'.
echo "Downloading sitemaps. . ."
for i in $SITEMAPS; do
wget --quiet -O - $URL/$i | grep -E -o '.*' | sed -e 's///g' -e 's/<\/loc>//g' >> tocrawl.txt
done

## Open tocrawl.txt and for earch entry, do a CURL (open the webpage)
echo "Hang on.... I'm crawling!"
cat tocrawl.txt | while read line
do
## Default setting: time the curl command and show how long it takes to crawl a certain URL. If you don't want to show the time, please remote 'time' from the line below
time curl -A 'Cache warmer' -s -L $line > /dev/null 2>&1
## Default setting: showing the URL which is being crawled. If you don't want to show the URL, please comment the line below. Just add a # in front of the 'echo $line'
echo $line
done

## Delete tocrawl.txt
```

`rm -f tocrawl.txt`

Ontwikkeld in samenwerking met: Martijn Spiekman