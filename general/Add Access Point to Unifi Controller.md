Default SSH Login:
Username: ubnt
Password: ubnt

# 1. make sure the AP is running the latest (or 2.1.0+)
# if it’s not, do
# syswrapper.sh upgrade http://ip-of-controller:8080/dl/firmware/BZ2/version-of-ap-see-ref-table-below/firmware.bin # 2. make sure the AP is in factory default state
# if it’s not, do
# syswrapper.sh restore-default
# 3. ssh into the device and type
mca-cli
# the CLI interface:
set-inform http://ip-of-controller:8080/inform

set-inform http://213.189.2.84:8080/inform

set-inform http://cloud.wificontroller.nl:8080/inform
213.189.2.225
__________

Can I run UniFi Controller as a Windows Service?
Can I run UniFi Controller as a Windows Service? Yes, we added the support since 2.2.0.

Make sure UniFi is not running
Locate your java installation directory.
On 64-bit, it’s usually at “C:\Program Files (x86)\Java\jre6\bin”; otherwise “C:\Program Files\Java\jre6\bin”. (replace jre6 for jre7 if you have the latest Java)
Add the dir above to the PATH (from Computer->Properties->Advanced system settings)
run a command prompt (as an Administrator, right click on ‘Command Prompt’ and choose ‘Run as administrator’. THIS IS IMPORTANT ON WIN2008/7+, otherwise the service may not get created)
cd (cd “%userprofile%/Ubiquiti Unifi” will usually do the trick, including the quote marks) java -jar lib\ace.jar installsvc
Start the service: net start “Unifi Controller”

Note: there’s a known bug against 2.2.X. The service shuts down the the administrator logs out.
___________