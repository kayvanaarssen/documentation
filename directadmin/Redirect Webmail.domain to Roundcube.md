# 1) You'll need to setup the virtualhost for apache.  This can be accomplished by adding a 2nd virtualhost along side the domains main one.

```
cd /usr/local/directadmin/data/templates
cp virtual_host2.conf custom
cd custom
```

>You'll need to edit the newly copied virtual_host2.conf file and add the blue to the end of whatever you currently have (the contents may vary)
>Just be sure to add the new VirtualHost below the existing one.

```
|?DOCROOT=`HOME`/domains/`DOMAIN`/public_html|
|?OPEN_BASEDIR_PATH=`HOME`/:/tmp:/usr/local/lib/php/|
<VirtualHost |IP|:|PORT_80| |MULTI_IP|>
|CUSTOM|
|?CGI=ScriptAlias /cgi-bin/ `DOCROOT`/cgi-bin/|
  ServerName www.|DOMAIN|
  ServerAlias www.|DOMAIN| |DOMAIN| |SERVER_ALIASES|
  ServerAdmin |ADMIN|
  DocumentRoot |DOCROOT|
  |CGI|

...
...

</VirtualHost>
<VirtualHost |IP|:|PORT_80| |MULTI_IP|>
  ServerName webmail.|DOMAIN|
  ServerAdmin |ADMIN|
  DocumentRoot /var/www/html/squirrelmail
  CustomLog /var/log/httpd/domains/|DOMAIN|.bytes bytes
  CustomLog /var/log/httpd/domains/|DOMAIN|.log combined
  ErrorLog /var/log/httpd/domains/|DOMAIN|.error.log
</VirtualHost>
```

## Save and exit.  Then run

```
echo "action=rewrite&value=httpd" >> /usr/local/directadmin/data/task.queue 
/usr/local/directadmin/dataskq d
```

to rewrite the httpd.conf files.

# 2) You'll also need to setup the dns portition.

```
cd /usr/local/directadmin/data/templates
cp dns_a.conf custom
cd custom
echo "webmail=|IP|" >> dns_a.conf
```

This will setup the webmail A record for new dns zones.  For existing dns zones, you'll have to manually add the webmail A record to point to the domains IP.

Create a new file:

`nano dns_a.conf`

Add the following:

```
localhost=127.0.0.1
|*if IS_IPV6!="yes"|
|DOMAIN|.=|IP|
mail=|IP|
pop=|IP|
www=|IP|
ftp=|IP|
smtp=|IP|
|*endif|
webmail=|IP|
```

# Virtual Host Example (Server1900 reference)

```
|?DOCROOT=`HOME`/domains/`DOMAIN`/public_html|
|?REALDOCROOT=`HOME`/domains/`DOMAIN`/public_html|

|?OBDP1=|
|*if PHP1_RELEASE!="0.000000"|
|?OBDP1=:/usr/local/php`PHP1_RELEASE`/lib/php/|
|*endif|
|?OBDP2=|
|*if PHP2_RELEASE!="0.000000"|
|?OBDP2=:/usr/local/php`PHP2_RELEASE`/lib/php/|
|*endif|

|?OPEN_BASEDIR_PATH=`HOME`/:/tmp:/var/tmp:/usr/local/lib/php/`OBDP1``OBDP2`|

|?FASTCGI_OPENBASEDIR=|
|*if OPEN_BASEDIR_ENABLED="ON"|
|?FASTCGI_OPENBASEDIR=-d open_basedir="`OPEN_BASEDIR_PATH`"|
|*endif|

|?PHP_MAIL_LOG=|
|?CLI_PHP_MAIL_LOG=|
|*if PHP_MAIL_LOG_ENABLED="1"|
|?PHP_MAIL_LOG=-d mail.log="`HOME`/.php/php-mail.log"|
|?CLI_PHP_MAIL_LOG=php_admin_value mail.log `HOME`/.php/php-mail.log|
|*endif|

|?PHP_EMAIL=`USER`@`DOMAIN`|
|?FASTCGI_SENDMAIL_FROM=-d sendmail_from="`PHP_EMAIL`"|

|?ALLOW_OVERRIDE=AllowOverride AuthConfig FileInfo Indexes Limit Options=Indexes,Includes,IncludesNOEXEC,MultiViews,SymLinksIfOwnerMatch,FollowSymLinks,None|

<VirtualHost |IP|:|PORT_80| |MULTI_IP|>
|CUSTOM|
|?CGI=ScriptAlias /cgi-bin/ `DOCROOT`/cgi-bin/|
	ServerName www.|DOMAIN|
	ServerAlias www.|DOMAIN| |DOMAIN| |SERVER_ALIASES|
	ServerAdmin |ADMIN|
	DocumentRoot |DOCROOT|
	|CGI|

	|USECANONICALNAME|

	<IfModule !mod_ruid2.c>
		SuexecUserGroup |USER| |GROUP|
	</IfModule>
	<IfModule mod_ruid2.c>
		RMode config
		RUidGid |USER| |GROUP|
		#RGroups apache |SECURE_ACCESS_GROUP|
		RGroups @none
	</IfModule>
	CustomLog /var/log/httpd/domains/|DOMAIN|.bytes bytes
	CustomLog /var/log/httpd/domains/|DOMAIN|.log combined
	ErrorLog /var/log/httpd/domains/|DOMAIN|.error.log

	|*if SUSPENDED_REASON|
	<IfModule mod_env.c>
		SetEnv reason |SUSPENDED_REASON|
	</IfModule>
	|*endif|

|*if HAVE_PHP1_FPM="1"|
	<IfModule mod_fastcgi.c>
		<FilesMatch "\.php$">
			SetHandler php|PHP1_RELEASE|-fcgi
		</FilesMatch>
		FastCgiExternalServer |REALDOCROOT|/php|PHP1_RELEASE|-fpm -idle-timeout 300 -socket /usr/local/php|PHP1_RELEASE|/sockets/|USER|.sock -pass-header Authorization -user |USER| -group |GROUP|
		Alias /php|PHP1_RELEASE|-bin |REALDOCROOT|/php|PHP1_RELEASE|-fpm
	</IfModule>
|*endif|

|*if HAVE_PHP2_FPM="1"|
	<IfModule mod_fastcgi.c>
		<FilesMatch \.php|PHP2_RELEASE|$>
			SetHandler php|PHP2_RELEASE|-fcgi
		</FilesMatch>
		FastCgiExternalServer |REALDOCROOT|/php|PHP2_RELEASE|-fpm -idle-timeout 300 -socket /usr/local/php|PHP2_RELEASE|/sockets/|USER|.sock -pass-header Authorization -user |USER| -group |GROUP|
		Alias /php|PHP2_RELEASE|-bin |REALDOCROOT|/php|PHP2_RELEASE|-fpm
	</IfModule>
|*endif|

	<Directory |DOCROOT|>
|*if CGI=""|
		|ALLOW_OVERRIDE|
		Options -ExecCGI
|*endif|
|*if HAVE_PHP1_FPM="1"|
		<IfModule mod_rewrite.c>
			RewriteEngine on
			RewriteOptions InheritBefore
			RewriteBase /
			RewriteCond %{REQUEST_URI} ^/php|PHP1_RELEASE|-bin(.*)
			RewriteRule . - [L]
		</IfModule>
|*endif|
|*if HAVE_PHP2_FPM="1"|
		<IfModule mod_rewrite.c>
			RewriteEngine on
			RewriteOptions InheritBefore
			RewriteBase /
			RewriteCond %{REQUEST_URI} ^/php|PHP2_RELEASE|-bin(.*)
			RewriteRule . - [L]
		</IfModule>
|*endif|
|*if HAVE_PHP1_FCGI="1"|
		<IfModule mod_fcgid.c>
			FCGIWrapper '/usr/local/safe-bin/fcgid|PHP1_RELEASE|.sh /usr/local/directadmin/data/users/|USER|/php/|DOMAIN|.ini |FASTCGI_SENDMAIL_FROM| |FASTCGI_OPENBASEDIR| |PHP_MAIL_LOG|' .php
			<FilesMatch "\.php$">
				SetHandler fcgid-script
				Options +ExecCGI
			</FilesMatch>
		</IfModule>
|*endif|
|*if HAVE_PHP2_FCGI="1"|
		<IfModule mod_fcgid.c>
			FCGIWrapper '/usr/local/safe-bin/fcgid|PHP2_RELEASE|.sh /usr/local/directadmin/data/users/|USER|/php/|DOMAIN|.ini |FASTCGI_SENDMAIL_FROM| |FASTCGI_OPENBASEDIR| |PHP_MAIL_LOG|' .php|PHP2_RELEASE|
			<FilesMatch "\.php|PHP2_RELEASE|$">
				SetHandler fcgid-script
				Options +ExecCGI
			</FilesMatch>
		</IfModule>
|*endif|
|*if HAVE_SAFE_MODE="1"|
		php_admin_flag safe_mode |SAFE_MODE|
|*endif|
|*if CLI="1"|
		php_admin_flag engine |PHP|
		php_admin_value sendmail_path '/usr/sbin/sendmail -t -i -f |PHP_EMAIL|'
		|CLI_PHP_MAIL_LOG|
|*endif|
|*if OPEN_BASEDIR="ON"|
		php_admin_value open_basedir |OPEN_BASEDIR_PATH|
|*endif|
|*if SUPHP="1"|
		suPHP_Engine |PHP|
		suPHP_UserGroup |USER| |GROUP|
|*endif|
	</Directory>
|*if HAVE_PHP_FCGI="1"|
	RewriteEngine on
	RewriteOptions inherit
|*endif|

|HANDLERS|
|MIMETYPES|

</VirtualHost>
<VirtualHost |IP|:80>
  ServerName webmail.|DOMAIN|
  ServerAdmin |ADMIN|
  DocumentRoot /var/www/html/roundcube
  CustomLog /var/log/httpd/domains/|DOMAIN|.bytes bytes
  CustomLog /var/log/httpd/domains/|DOMAIN|.log combined
  ErrorLog /var/log/httpd/domains/|DOMAIN|.error.log
</VirtualHost>
```

# CHOWN The created files as Directadmin

`chown diradmin:diradmin *`

