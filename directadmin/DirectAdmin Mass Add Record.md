# Mass Change / Add Records in DirectAdmin

Create a file `nano ~/mass-change.sh`

Add the following code and change it if needed

```
#!/bin/sh
DATAU=/usr/local/directadmin/data/users
for u in `ls $DATAU`; do
{
IP=`grep ip= $DATAU/$u/user.conf | cut -d= -f2`
for d in `cat $DATAU/$u/domains.list $DATAU/$u/domains/*.pointers 2>/dev/null | cut -d= -f1`; do
{
echo "adding new record with IP $IP to $d";
echo "mandrill._domainkey IN TXT \"v=DKIM1; k=rsa; p=MIGfMA0GCSqGSIb3DQEBAQUAA4GNADCBiQKBgQCrLHiExVd55zd/IQ/J/mRwSRMAocV/hMB3jXwaHH36d9NaVynQFYV8NaWi69c1veUtRzGt7yAioXqLj7Z4TeEUoOLgrKsn8YnckGs9i3B3tVFB+Ch/4mPhXWiNfNdynHWBcPcbJ8kjEQ2U8y78dHZj1YeRXXVvWob2OaKynO8/lQIDAQAB;"\" >> /var/named/${d}.db
};
done;
};
done;
exit 0;
```

`chmod -x mass-change.sh`

Run the code:

`./mass-change.sh`

# Ander voorbeeld:

```
#!/bin/sh
DATAU=/usr/local/directadmin/data/users
for u in `ls $DATAU`; do
{
      IP=`grep ip= $DATAU/$u/user.conf | cut -d= -f2`
      for d in `cat $DATAU/$u/domains.list $DATAU/$u/domains/*.pointers 2>/dev/null | cut -d= -f1`; do
      {
            echo "adding new record with IP $IP to $d";
            echo "mynewrecord   14400    IN   A   $IP" >> /var/named/${d}.db
      };
      done;
};
done;
exit 0;
```
