# Checken van welke map iets aangesproken wordt:

`tail -n 500 -f /etc/virtual/usage/%username%.bytes`

Check welke php bestanden de afgelopen 7 dagen aangepast zijn:

`find . -type f -name '*.php' -mtime -7`

## Om de IP’s zelf te checken (voor als ik er niet ben ofzo), kun je het volgende doen:

>opzoeken welk bestand er spam verstuurd (bijv. die start.php)

`cd /var/log/httpd/domains/`

Voer uit: 

`find . | xargs grep ‘start.php’`

>Je krijgt dan een hele output aals het goed is.
>Op elke regel staat een IP-adres. Die kun blokkeren in csf via DirectAdmin of via SSH:

Om via SSH te blokkeren: 

`csf -d IP_ADRES`

## Leeghalen van Mailqueue:

`exim -bp|grep "USERNAME"| awk {'print $3'}| xargs exim -Mrm`

Bestand localiseren:

Eerst: `updatedb` uitvoeren

`locate error.php | grep USERNAME`

## IP Adres of dergelijke vinden in files:

`find . | xargs grep "CONTENT" -s`