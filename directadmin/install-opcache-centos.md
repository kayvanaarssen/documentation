# Install Opcache Directadmin CentOS

>What is OP CACHE ?

>You can see opcache as the big brother of apc cache with is depraced since PHP 5.3

>The Zend OpCache provides faster PHP execution through opcode caching and optimization. It improves PHP performance by storing precompiled script bytecode in the shared memory.

## How to install OPCACHE on Directadmin
Requirments:   Custombuild 2.0 and PHP 5.5 or higher.

```
cd /usr/local/directadmin/custombuild 
./build set opcache yes 
./build opcache
```

Create a .php script to monitor cache hits

https://raw.githubusercontent.com/rlerdorf/opcache-status/master/opcache.php


Location to change settings of OPCACHE:

`/usr/local/lib/php.conf.d/10-directadmin.ini`