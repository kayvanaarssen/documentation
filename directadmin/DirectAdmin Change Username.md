# This is an old thread but it came up in a search to change a username. There IS a way to do it:

If you’ve got a client who wants to have a different username, then you change it by typing

```
cd /usr/local/directadmin/scripts
./change_username.sh olduser newuser
```

(From http://help.directadmin.com/item.php?id=193)

Indien je het wachtwoord van het useraccount niet meer weet. Verander deze dan onder admin > Change passwords

Daarna de mail overzetten door via de FTP in te loggen en dan de map “Maildir” downloaden naar je computer.

Daarna een nieuw mail account aanmaken.

Dan in de FTP IMAP > mailaccount > Maildir de mail weer terug zetten