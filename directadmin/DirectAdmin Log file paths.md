# Log File paths

>The first place you should go when trying to debug a problem is the log file for that program.   The list of Log Files are as follows:

## DirectAdmin:

```
/var/log/directadmin/error.log
/var/log/directadmin/errortaskq.log
/var/log/directadmin/system.log
/var/log/directadmin/security.log
```

## Apache:

```
/var/log/httpd/error_log
/var/log/httpd/access_log
/var/log/httpd/suexec_log
/var/log/httpd/fpexec_log
/var/log/httpd/domains/domain.com.error.log
/var/log/httpd/domains/domain.com.log
/var/log/messages (generic errors)
```

## Proftpd:

```
/var/log/proftpd/access.log
/var/log/proftpd/auth.log
/var/log/messages (generic errors)
```

### PureFTPd:

```
/var/log/pureftpd.log
```

## Dovecot and vm-pop3d:

```
/var/log/maillog
/var/log/messages
```

## named (bind):

```
/var/log/messages
```

## exim:

```
/var/log/exim/mainlog
/var/log/exim/paniclog
/var/log/exim/processlog
/var/log/exim/rejectlog
```

> (on FreeBSD, they have "exim_" in front of the filenames)

## mysqld:

### RedHat:

`/var/lib/mysql/server.hostname.com.err`

### FreeBSD and Debian:

`/usr/local/mysql/data/server.hostname.com.err`

## crond:

```
/var/log/cron
```

## To view a log file, run:

`less /var/log/filename`

> Where /var/log/filename is the path of the log you wish to view.  If the log is too large you can use the "tail" command:

`tail -n 30 /var/log/filename`

>Where 30 is the number of lines from the end you wish to view.