# For RedHat/CentOS:

```
cd /var/named
perl -pi -e 's#v=spf1 a mx \ip4:1.2.3.4#v=spf1 a mx \ip4:2.3.1.4#' *.db
echo "action=rewrite&value=named" >> /usr/local/directadmin/data/task.queue
```

# For Debian/Ubuntu:

```
cd /etc/bind
perl -pi -e 's#v=spf1 a mx \ip4:1.2.3.4#v=spf1 a mx \ip4:2.3.1.4#' *.db
echo "action=rewrite&value=named" >> /usr/local/directadmin/data/task.queue
```

# For FreeBSD:


```
cd /etc/namedb
perl -pi -e 's#v=spf1 a mx \ip4:1.2.3.4#v=spf1 a mx \ip4:2.3.1.4#' *.db
echo "action=rewrite&value=named" >> /usr/local/directadmin/data/task.queue
```

```
cd /var/named
perl -pi -e 's#v=spf1 a mx \ip4:1.2.3.4#v=spf1 a mx \ip4:2.3.1.4#' *.db
echo "action=rewrite&value=named" >> /usr/local/directadmin/data/task.queue

sed -i ‘s/ip4:123.123.123.123/ip4:123.123.123.123 include:spf.mandrillapp.com/g’ *.db
```
