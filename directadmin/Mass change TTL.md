>When changing the IP of a domain, end users will have the old IP of the domain cached at their ISP's nameservers for a duration of the TTL (time to live).  The default value is 14400 seconds (4 hours).  This means, that when you change the IP of the sever, the worst case, is the end users will be using the wrong IP for 4 hours before the cache expires and the IP is recached with the correct value.

>The simple way to minimize this propagation error is to lower the TTL.   We'll change the TTL from 14400 to 100 seconds for all domains on your system (assuming server move).  Run the following:

```
cd /usr/local/directadmin/data/templates
perl -pi -e 's/14400/100/' named.db
echo "action=rewrite&value=named" >> /usr/local/directadmin/data/task.queue
```

>Wait about a minute or 2.  Check your /var/named/*.db files.. the TTL should now be 100 instead of 14400.  Once complete, do the same thing, but replace 100 with 14400 and 14400 with 100 in the perl command.

>If you are running the for just one domain, then run the perl command in /var/named/domain.com.db, instead of the named.db file and restart named.

>In both cases, you'll want to do this at least 4 hours before you actually do the IP change so that all cached values are stored for no more than 100 seconds.  The nameservers will then query for the new IP every 100 seconds (instead of 4 hours), giving the end user the new IP much quicker.

>*** Note: On Debian systems, replace /var/named with /etc/bind.
>On FreeBSD systems, replace /var/named with /etc/namedb.