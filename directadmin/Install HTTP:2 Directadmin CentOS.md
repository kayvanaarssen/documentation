Install HTTP/2 Directadmin CentOS

>>It's most likely that your OpenSSL does not support ALPN extension, that's why it's difficult to get HTTP/2 running with Apache. Please follow these steps to get HTTP/2 enabled with Apache:

# FOR CENTOS 7 with building issues:

Execute: 

`rm -rf /usr/local/lib_da`

## 1) Install OpenSSL, with ALPN support:


```
wget ftp://ftp.openssl.org/source/openssl-1.0.2j.tar.gz
tar xzf openssl-1.0.2j.tar.gz
cd openssl-1.0.2j
./config --prefix=/usr/local/lib_http2 no-ssl2 no-ssl3 zlib-dynamic -fPIC
make depend
make install
```

## 2) Install nghttp2:


```
cd /usr/local/directadmin/custombuild
./build update
./build nghttp2
```

## 3) Enable http2 in Apache:


```
cd /usr/local/directadmin/custombuild
mkdir -p custom/ap2
cp -p configure/ap2/configure.apache custom/ap2/configure.apache
```

Edit custom/ap2/configure.apache, find:


`"--with-ssl=/usr" \`


Replace with:


```
"--enable-http2" \
"--enable-ssl-staticlib-deps" \
"--with-ssl=/usr/local/lib_http2" \
```


Re-install apache:


`./build apache`


## 4) Configure Apache to run http2:


```
echo 'ProtocolsHonorOrder On' >> /etc/httpd/conf/extra/httpd-includes.conf
echo 'Protocols h2 h2c http/1.1' >> /etc/httpd/conf/extra/httpd-includes.conf
```

Update SSL Ciphers in `/etc/httpd/conf/extra/httpd-ssl.conf`:

```
cd /usr/local/directadmin/custombuild
./build rewrite_confs
```

>>Restart apache now :) It's recommended to run PHP in PHP-FPM mode, so that PHP wouldn't be installed as a module of Apache (otherwise you might have openssl library conflict, if PHP is compiled as apache module and uses the old (OS) version of openssl).

>>To enable HTTP/2 in cURL, add the following code to /usr/local/directadmin/custombuild/custom/curl/configure.curl:

```
#!/bin/sh
LIBS="-ldl" ./configure --with-nghttp2=/usr/local --with-ssl=/usr/local/lib_http2
```

And execute:

```
cd /usr/local/directadmin/custombuild
chmod 700 custom/curl/configure.curl
./build curl
```

# To enable HTTP/2 in Nginx:

```
cd /usr/local/src/
wget ftp://ftp.openssl.org/source/openssl-1.0.2j.tar.gz
tar xzf openssl-1.0.2j.tar.gz
```

For standalone Nginx:

```
cd /usr/local/directadmin/custombuild
mkdir -p custom/nginx
cp -p configure/nginx/configure.nginx custom/nginx/configure.nginx
```

For Nginx_Apache:

```
cd /usr/local/directadmin/custombuild
mkdir -p custom/nginx_reverse
cp -p configure/nginx_reverse/configure.nginx custom/nginx_reverse/configure.nginx
```

Build Nginx with statically linked OpenSSL library:
1) Edit configure.nginx which was copied to custom/ folder
2) Add "--with-openssl=/usr/local/src/openssl-1.0.2j" and "--with-http_v2_module" flags
3) Run "./build nginx"
4) Execute:

```
cd /usr/local/directadmin/data/templates/
cp -fp nginx_server_secure.conf custom/nginx_server_secure.conf
cp -fp nginx_server_secure_sub.conf custom/nginx_server_secure_sub.conf
perl -pi -e 's#listen \|IP\|:\|PORT_443\| ssl#listen |IP|:|PORT_443| ssl http2#g' custom/nginx_server_secure.conf custom/nginx_server_secure_sub.conf
cd /usr/local/directadmin/custombuild
./build rewrite_confs
``