`exim -bp|grep “username”| awk {‘print $3’}| xargs exim –Mrm`

>We have already discussed the basic command usages of Exim mail server in one of the previous post. In this post I am explaining the command usages for finding or sorting emails in Exim mail queue for particular sender or receiver. We can sort it out in different ways, by using exim basic command and also by using the command “exiqgrep”. To sort mail queue for a sender or receiver, you must have the idea about the field in Exim command output. Then you can simply sort it out with the help of “awk” and other Linux basic commands.

The default Exim command output field with details are explained below:

The 'exim -bp' command output:

```
4d 2.3K 1X0Baa-0006iR-Ml <>
fax@145.144-238-87.ripe.rewwwwwfs.net

70h 999 1X0RmS-0002Ue-Fe > email@example.com
```

>In the above list, there are two mails in the mail queue. The queue field details are explained below: 1st field : Message’s age. (Eg: 4d : Four day)
>2nd field : Size. (2.3k)
>3rd field : Message ID (1X0Baa-0006iR-Ml)
>4th field : sender. (info@hxxxxxer.com)
>5th field/second line : recipient address. (email@example.com)

>It’s really simple to find/sort email details for a particular user (Sender or Receiver) from the Exim mail queue. We have already discussed about the Exim mail server in different way. Here I am explaining different option to sort emails for a User.

## Method I :

>Basic method with “exim” command.
>We can sort the emails by using the exim command, see the examples below:

I: List all emails from a particular sender:

`exim -bp|grep “username”`

Where “username” is the sender name.

Example:

`exim -bp|grep olne5`

```
43h 3.6K 1WFLFH-0006uA-Gk > 43h 3.6K 1WFLOL-0000LX-97 > 43h 1.9K 1WFLQG-0000hv-5M > 43h 3.6K 1WFM7W-0001T5-7B > 42h 6.0K 1WFMEn-0002yJ-A8 > 42h 3.7K 1WFMGq-0003Sf-4T > 42h 3.5K 1WFMyn-0002yN-Tt >
```

###########

Deleting email from the exim queue is unfortunately not that simple. If you have a massive spammer in your system, you can clear the email originating from them with the command below.

`exim -bpru | tr ‘\n’ + | sed -e “s/++/=/g” | tr -d + | tr = ‘\n’ | grep “spammer@email.com” | awk {‘print $3’} | xargs exim -Mrm`

>Simple replace the spammer@email.com address with the potential spammer.
>Don’t forget that this command uses the whole queue list to process. Sometimes if you have hundreds of thousands of email from this spammer in queue, it will get hard to process them all for the system. So instead of using the whole queue list, we can break it down to pieces with the head command.

The command below will break it down to pieces of 5000, so if you have more than 5000, you should run this command several times.

`exim -bpru | head -n 5000 | tr ‘\n’ + | sed -e “s/++/=/g” | tr -d + | tr= ‘\n’ | grep “spammer@email.com” | awk {‘print $3’} | xargs exim -Mrm`

# To delete all mails from the queue, simply use this command.

`exim -bp | awk ‘/^ *[0-9]+[mhd]/{print “exim -Mrm ” $3}’ | sh`

Bron: http://www.plugged.in/uncategorized/delete-mail-from-queue-in-exim.html