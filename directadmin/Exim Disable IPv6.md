# Check

`cat /var/log/exim/mainlog`

or

`tail -n 500 /var/log/exim/mainlog`

```
=============

-bash-3.2# grep 1W6OuM-0005cl-J8 /var/log/exim_mainlog
2014-01-23 19:21:42 1W6OuM-0005cl-J8 <= root@host.xxxx. U=root P=local S=350 T=”test mail” for test@gmail.com
2014-01-23 19:21:42 cwd=/var/spool/exim 4 args: /usr/sbin/exim -v -Mc 1W6OuM-0005cl-J8
2014-01-23 19:21:42 1W6OuM-0005cl-J8 gmail-smtp-in.l.google.com [xxxx:abcd:xxxx:xab::xa] Network is unreachable
2014-01-23 19:21:43 1W6OuM-0005cl-J8 Completed
============
```

>You can see that exim is trying to send outgoing emails via IPv6 . It happens if the recipient server supports it, ( gmail supports it ) as a result mail delivery gets affected or the mails reach junk/spam folder.

>If IPv6 delivery is not intended and DNS records for the same are not configured, then the recipient SMTP server would not be able to obtain a reverse DNS entry of the sending IP ( IP in IPv6 ) and as a result it affects the mail delivery.

>To get around this, either configure your IPv6 DNS entries or just force exim to send mails only via IPv4 by adding the below line to the exim config file ( /etc/exim.conf ) 

Edit `nano /etc/exim.conf`

`disable_ipv6 = true`

Finally restart exim.