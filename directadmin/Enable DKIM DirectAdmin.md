# Enable DKIM Directadmin and Exim

Relating to this feature, this guide will outline how to install DKIM on your DirectAdmin server.

DKIM will sign outbound message, and receiving servers will check that signature against DNS records added to your zone to verify the signature and lower the spam score if it passes.

1) Ensure the exim supports DKIM signging:

```
[root@es5 ~]# /usr/sbin/exim -bV | grep 'Support for'
Support for: crypteq IPv6 Perl OpenSSL move_frozen_messages Content_Scanning DKIM Old_Demime PRDR OCSP
```

>where we want to see DKIM in the list.
>If exim does not support DKIM, then re-compile exim.

2) Add it to exim

```
cd /etc
wget -O exim.dkim.conf http://files.directadmin.com/services/exim.dkim.conf
```

Then edit your `/etc/exim.conf`, and find the code:

`remote_smtp:
 driver = smtp`

and change it to look like

`remote_smtp:
 driver = smtp
 .include_if_exists /etc/exim.dkim.conf`

and restart exim

`/etc/init.d/exim restart

3) Turn in on in DirectAdmin.

```
cd /usr/local/directadmin
cp -f conf/directadmin.conf conf/directadmin.conf.backup
echo 'dkim=1' >> conf/directadmin.conf
```

>Making absolutely sure that you're using two >> characters, else you'll empty your directadmin.conf.

And confirm it's set, and restart DA:

```
[root@es5 directadmin]# ./directadmin c | grep dkim
dkim=1
[root@es5 directadmin]# /etc/init.d/directadmin restart
Stopping DirectAdmin:                                      [  OK  ]
Starting DirectAdmin:                                      [  OK  ]
```

4) At this point, any domain created after the change should have the DKIM keys created, and dns zones updated.

>For existing domains, you can either enable it individually for each domain, one-by-one:

```
cd /usr/local/directadmin/scripts
./dkim_create.sh domain.com
```

5) or you can enable it for all of your domains like this:

`echo "action=rewrite&value=dkim" >> /usr/local/directadmin/data/task.queue`

>but it may be a good idea to test it out manually with 3) first, to ensure it works correctly.

>Note: the dkim_create.sh script itself doesn't touch the zone.  It uses a task.queue entry to have the dataskq add the dns entry based on the keys that the dkim_create.sh script created.  As such, it may take up to one minute for the records to be added.

>Important: If any of your domains are hosted using an external DNS server that DA does not control, you MUST manually add the TXT records to the remote zones.  You can copy them over as needed.  If the DNS does not have the records, but your emails are signed, this may increase the spam score of those emails, which is the opposite of what we want.