# If you need to change the IP that is used to send email out of your systme, you can do so by editing your 

Edit:

`nano /etc/exim.conf`

Change:

`remote_smtp:
  driver = smtp`

to:

`remote_smtp:
  driver = smtp
  interface = 1.2.3.4`

Where 1.2.3.4 is the IP you want exim to use.

>You'll also need to update the SPF values in all TXT records in the dns zone for all domains that send from your server.
>The SPF value must match the sending IP, which will be the new value you set.

>SpamBlocker 4.3.1+ and DirectAdmin 1.47.0 has the ability to do this automatically based on the owned IP:

http://www.directadmin.com/features.php?id=1692

## Mass Change SPF

```
cd /var/named
perl -pi -e 's#v=spf1 a mx \ip4:84.22.111.26#v=spf1 a mx \ip4:84.22.112.15#' *.db
echo "action=rewrite&value=named" >> /usr/local/directadmin/data/task.queue
```

perl -pi -e 's#v=spf1 a mx \ip4:1.2.3.4#v=spf1 a mx \ip4:2.3.1.4#' *.db