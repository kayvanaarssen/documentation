Go to Virtual Machine > Install VMware Tools (or VM > Install VMware Tools).

Note: If you are running the light version of Fusion, or a version of Workstation without VMware Tools, or VMware Player, you are prompted to download the Tools before they can be installed. Click Download Now to begin the download.

In the Ubuntu guest, run these commands:

Run this command to create a directory to mount the CD-ROM:

`sudo mkdir /mnt/cdrom`

When prompted for a password, enter your Ubuntu admin user password.

Note: For security reasons, the typed password is not displayed. You do not need to enter your password again for the next five minutes. 

Run this command to mount the CD-ROM:

`sudo mount /dev/cdrom /mnt/cdrom`

or

`sudo mount /dev/sr0 /mnt/cdrom`

The file name of the VMware Tools bundle varies depending on your version of the VMware product. Run this command to find the exact name:

`ls /mnt/cdrom`

Run this command to extract the contents of the VMware Tools bundle:

`tar xzvf /mnt/cdrom/VMwareTools-x.x.x-xxxx.tar.gz -C /tmp/`

Note: x.x.x-xxxx is the version discovered in the previous step.

Run this command to change directories into the VMware Tools distribution:

`cd /tmp/vmware-tools-distrib/`

Run this command to install VMware Tools:

`sudo ./vmware-install.pl -d`

Note: The -d switch assumes that you want to accept the defaults. If you do not use -d, press Return to accept each default or supply your own answers.

Run this command to reboot the virtual machine after the installation completes:

`sudo reboot`