# Install VMWARE Tools - CentOS 6.X

https://www.vmware.com/support/ws5/doc/ws_newguest_tools_linux.html

First we need to update the server

`yum update -y`

Next we install the necessary packages.

`yum install nano wget perl -y`

Next we create a dir /mnt/cdrom

`mkdir /mnt/cdrom`

Mount the Virtual drive:

`mount /dev/cdrom /mnt/cdrom`

Change the current dir

`cd /tmp`

Extract the Tar file

`tar zxf /mnt/cdrom/VMwareTools-5.0.0-<xxxx>.tar.gz`

Unmount the virtual Drive

`umount /dev/cdrom`

Run the installer:

`cd vmware-tools-distrib
./vmware-install.pl`

> Respond to the configuration questions on the screen. Press Enter to accept the default value. 
> Log off of the root account.

`exit`

Reboot the machine

`reboot`