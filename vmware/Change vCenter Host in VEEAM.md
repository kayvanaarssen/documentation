# Solution

## This solution applies ONLY if the vCenter Server database has not changed.
> If the Name/FQDN/IP of the vCenter changed due to a reinstall or upgrade and a new vCenter database was used, the Ref-IDs will have changed. Due to the changed Ref-IDs you will need to follow the documented process in www.veeam.com/KB1299 to resolve the "Object "VM_Name" not found" errors if the original jobs are kept.
 
> Before starting you will have to find out the Name\FQDN\IP Veeam is using to communicate with the VC presently. To do this, edit the entry for the VC under Backup Infrastructure and note the “DNS name or IP address”.

Next perform the following steps, checking that same location again afterward to confirm the change.
 
1.  Launch PowerShell from inside of Veeam Backup & Replication menu

2.  Run the following command in the PowerShell window that has now opened.

`$Servers = Get-VBRServer -name "Current-VCName/IP"`

3.  Run the following command next.

`$Servers.SetName("New-VCName/IP")`

4.  Close and reopen the Veeam Backup & Replication console to update the entry within the GUI

Source Link: https://www.veeam.com/kb1905